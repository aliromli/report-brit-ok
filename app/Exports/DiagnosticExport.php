<?php
namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class DiagnosticExport implements FromCollection, WithHeadings
{
    use Exportable;
	
    
    protected $data;
    
	public function __construct($data='')
    {
        $this->data = $data;
    }

    public function collection()
    {
        $kp = $this->sql();
		
		if($kp!=null)
		{
			$a1 = array();
			$k = 0;
			foreach($kp as $data){
				
    
				$a1[]= [
						'terminal_ext_id'=> $data->terminal_ext_id,
						'terminal_id' => $data->terminal_id,
						'terminal_sn' => $data->terminal_sn,
						'battery_temp' => $data->battery_temp,
						'battery_percentage' => $data->battery_percentage,
						'latitude' => $data->latitude,
						'longitude' => $data->longitude,
						'meid' => $data->meid,
						'switching_times' => $data->switching_times,
						'swiping_card_times' => $data->swiping_card_times,
						'dip_inserting_times' => $data->dip_inserting_times,
						'nfc_card_reading_times' => $data->nfc_card_reading_times,
						'front_camera_open_times' => $data->front_camera_open_times,
						'rear_camera_open_times' => $data->rear_camera_open_times,
						'charge_times' => $data->charge_times,
						'total_memory' => $data->total_memory,
						'available_memory' => $data->available_memory,
						'total_flash_memory' => $data->total_flash_memory,
						'available_flash_memory' => $data->available_flash_memory,
						'total_mobile_data' => $data->current_boot_time,
						'total_boot_time' => $data->total_boot_time,
						'total_length_printed' => $data->total_length_printed,
						'installed_apps' => $data->installed_apps
						];
				
			}
			return collect($a1);
		}
		else
		{
			$a1 = array();
			
				
			$a1[]= [
						'terminal_ext_id'=> null,
						'terminal_id' => null,
						'terminal_sn' => null,
						'battery_temp' => null,
						'battery_percentage' => null,
						'latitude' => null,
						'longitude' => null,
						'meid' => null,
						'switching_times' =>null,
						'swiping_card_times' => null,
						'dip_inserting_times' => null,
						'nfc_card_reading_times' => null,
						'front_camera_open_times' => null,
						'rear_camera_open_times' => null,
						'charge_times' => null,
						'total_memory' => null,
						'available_memory' => null,
						'total_flash_memory' => null,
						'available_flash_memory' =>null,
						'total_mobile_data' => null,
						'total_boot_time' =>null,
						'total_length_printed' => null,
						'installed_apps' => null
					];
				
			
			return collect($a1);
			
		}	
		
		
    }

    public function headings(): array
    {
        return [
                        'terminal_ext_id',
						'terminal_id',
						'terminal_sn',
						'battery_temp',
						'battery_percentage',
						'latitude',
						'longitude',
						'meid',
						'switching_times',
						'swiping_card_times',
						'dip_inserting_times',
						'nfc_card_reading_times',
						'front_camera_open_times',
						'rear_camera_open_times',
						'charge_times',
						'total_memory',
						'available_memory',
						'total_flash_memory',
						'available_flash_memory',
						'total_mobile_data',
						'total_boot_time',
						'total_length_printed',
						'installed_apps'
           
        ];
    }
	
	public function sql(){
		
		return $this->data;
	
	}
	
	

}

?>