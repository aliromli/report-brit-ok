<?php
namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class TerminalReportExport implements FromCollection, WithHeadings
{
    use Exportable;
	
    
    protected $data;
    
	public function __construct($data='')
    {
        $this->data = $data;
    }

    public function collection()
    {
        $kp = $this->sql();
		
		if($kp!=null)
		{
			$a1 = array();
			$k = 0;
			foreach($kp as $data){
				
				$a1[]= [
						'sn'=> $data->sn,
						'imei'=> $data->imei,
						'terminal_id'=> $data->terminal_id,
						'merchant_Id'=>$data->merchant_id,
						'merchant_name1'=>$data->merchant_name1,
						'merchant_name2'=>$data->merchant_name2,
						'merchant_name3'=>$data->merchant_name3,
						'feature_sale'=>$this->bol($data->feature_sale),
						'feature_sale_tip'=>$this->bol($data->feature_sale_tip),
						'feature_sale_redemption'=>$this->bol($data->feature_sale_redemption),
						'feature_card_verification'=>$this->bol($data->feature_card_verification),
						'feature_sale_completion'=>$this->bol($data->feature_sale_completion),
						'feature_installment'=>$this->bol($data->feature_installment),
						'feature_sale_fare_non_fare'=>$this->bol($data->feature_sale_fare_non_fare),
						'feature_manual_key_in'=>$this->bol($data->feature_manual_key_in),
						'feature_qris'=>$this->bol($data->feature_qris),
						'feature_contactless'=>$this->bol($data->feature_contactless),

						'random_pin_keypad'=>$data->random_pin_keypad,
						'beep_pin_keypad'=>$data->beep_pin_keypad,
						'auto_logon'=>$this->bol($data->auto_logon),
						'next_logon'=>$data->next_logon,
						'installment1_options'=>$data->installment1_options,
						'installment2_options'=>$data->installment2_options,
						'installment3_options'=>$data->installment3_options,
						'state'=>$data->state,
						'app_version'=>$data->app_version,
						'launcher_version'=>$data->launcher_version,
						'vfs_version'=>$data->vfs_version,
						'vfss_version'=>$data->vfss_version,
						'ECR_version'=>'',
						'ECR_version'=>'',
						'ROM_version'=>'',
						'security_patch_version'=>'',
						'update_ts'=>'',
						'last_diagnostic_time'=>$data->last_diagnostic_time,
						'last_heartbeat_time'=>$data->last_heartbeat_time,
						'latitude'=>$data->latitude,
						'longitude'=>$data->longitude,
						'connection'=>'',	
						'wifi_strength'	=>'',
						'cell_name'	=>$data->cell_name,
						'cell_type'	=>$data->cell_type,
						'cell_strength'	=>$data->cell_strength,
						'push_logon' => $data->push_logon,
						'host_report' => $this->bol($data->host_report),
						'host_logging' => $this->bol($data->host_logging),
						'rom_version' => $data->rom_version,
						'sp_version' => $data->sp_version
 
						];
				
			}
			return collect($a1);
		}
		else
		{
			$a1 = array();
			
				
			$a1[]= [
						'SN'=> null,
						'Imei'=> null,
						'TerminalId'=>null,
						'merchant_id'=>null,
						'Merchant_name1'=>null,
						'Merchant_name2'=>null,
						'feature_sale'=>null,
						'feature_sale_tip'=>null,
						'feature_sale_redemption'=>null,
						'feature_card_verification'=>null,
						'feature_sale_completion'=>null,
						'feature_installment'=>null,
						'feature_sale_fare_non_fare'=>null,
						'feature_manual_key_in'=>null,
						'feature_qris'=>null,
						'feature_contactless'=>null,
						'random_pin_keypad'=>null,
						'beep_pin_keypad'=>null,
						'auto_logon'=>null,
						'next_logon'=>null,
						'installment1_options'=>null,
						'installment2_options'=>null,
						'installment3_options'=>null,
						'state'=>null,
						'app_version'=>null,
						'launcher_version'=>null,
						'vfs_version'=>null,
						'vfss_version'=>null,
						'ECR_version'=>null,
						'ECR_version'=>null,
						'ROM_version'=>null,
						'security_patch_version'=>null,
						'update_ts'=>null,
						'last_diagnostic_time'=>null,
						'last_heartbeat_time'=>null,
						'latitude'=>null,
						'longitude'=>null,
						'connection'=>'',	
						'wifi_strength'	=>'',
						'cell_name'	=>null,
						'cell_type'	=>null,
						'cell_strength'	=>null,
						'push_logon' => null,
						'host_report' =>null,
						'host_logging' =>null,
						'rom_version' =>null,
						'sp_version' =>null,
					];
				
			
			return collect($a1);
			
		}	
		
		
    }

    public function headings(): array
    {
        return [
            'SN',
            'Imei',
			'Terminal_id',
			'Merchant_id',
			'Merchant_name1',
			'Merchant_name2',
			'Merchant_name3',
			'feature_sale',
			'feature_sale_tip',
			'feature_sale_redemption',
			'feature_card_verification',
			'feature_sale_completion',
			'feature_installment',
			'feature_sale_fare_non_fare',
			'feature_manual_key_in',
			'feature_qris',
			'feature_contactless',
			'random_pin_keypad',
			'beep_pin_keypad',
			'auto_logon',
			'next_logon',
			'installment1_options',
			'installment2_options',
			'installment3_options',
			'state',
			'app_version',
			'launcher_version',
			'vfs_version',
			'vfss_version',
			'ECR_version',
			'ECR_version',
			'ROM_version',
			'security_patch_version',
			'update_ts',
			'last_diagnostic_time',
			'last_heartbeat_time',
			'latitude',
			'longitude',
			'connection',
			'wifi_strength',
			'cell_name',
			'cell_type',
			'cell_strength',
			'Push Logon',
			'Host Report',
			'Host Logging',
			'Rom Version',
			'Sp Version'
        ];
    }
	
	public function sql(){
		
		return $this->data;
	
	}

	public function bol($data)
	{
      if(isset($data))
	  {
		 return ($data==TRUE) ? "YES" : "NO";
	  }
	  else
	  {
		return "";
	  }
	}
	
	

}

?>