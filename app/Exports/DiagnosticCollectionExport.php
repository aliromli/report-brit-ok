<?php
namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;
use App\Models\DiagnosticReport;
use App\Models\Diagnostic;


class DiagnosticCollectionExport implements FromCollection
{
    use Exportable;
	
    public function collection()
    {
        return Diagnostic::lazy();
    }
}
