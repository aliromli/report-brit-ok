<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Traits\Uuid;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    use Uuid;
    protected $table = 'sec_user';
    //protected $primaryKey = 'id';

    // protected $fillable = [
    //     'name',
    //     'first_name',
    //     'last_name',
    //     'middle_name',
    //     'group_id',
    //     'login',
    //     'password',
    // ];
}
