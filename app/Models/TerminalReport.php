<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TerminalReport extends Model
{
    protected $table = 'tms_v_terminal_report';
    protected $primaryKey = 'sn';
    public $incrementing = false;
    
}
