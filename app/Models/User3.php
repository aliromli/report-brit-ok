<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Webpatser\Uuid\Uuid;
class User3 extends Authenticatable
{
    use Notifiable;
    // set to false, so we tell that primary-key field 
    // not auto incrementing anymore 
    public $incrementing = false;
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'sec_user';
    protected $primaryKey = 'id';
    protected $fillable = [
        'login', 'group_id', 'password',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = [
    //     'password', 'remember_token',
    // ];
    public static function boot()
    {
         parent::boot();
         self::creating(function($model){
             $model->id = self::generateUuid();
         });
    }
    public static function generateUuid()
    {
         return Uuid::generate();
    }
}