<?php

namespace App\Jobs;

use Illuminate\Support\Facades\DB;
use App\Models\DiagnosticReport;
use Exception;
//use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Maatwebsite\Excel\Facades\Excel;

	
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ExportDiag implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $fileName;
    protected $process;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($process, $fileName)
    {
        $this->fileName = $fileName;
        $this->process = $process;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    /*
    public function handle2()
    {
        try {
           
            //====== batas == 
            $filename = $this->fileName; // time()."-data-report-diagnotics.xls";
            $output=fopen(base_path().'/storage/app/'.$filename,'w+');
    
          
            fputcsv($output, [
                'No',
                'Terminal Ext Id', 
                'Terminal Id',
                'Terminal SN',
                'Battery Temp',
                'Battery Percentage',
                'Latitude',
                'Longitude',
                'Meid',
                'Switching times',
                'Swiping card times',
                'Dip Inserting Times',
                'Nfc Card reading times',
                'Front camera open times',
                'Rear Camera Open times',
                'Charge times',
                'Total memory',
                'Available memory',
                'Total Flash Memory',
                'Available Flash Memory',
                'Total Mobile data',
                'Total Boot time',
                'Total Length printed',
                'Installed Apps'
                ],
                "\t",'"');
                $chunk = 100000;
                DiagnosticReport::select(
                    'terminal_ext_id',
                    'terminal_id', 
                    'terminal_sn',
                    'battery_temp',
                    'battery_percentage',
                    'latitude',
                    'longitude',
                    'meid',
                    'switching_times',
                    'swiping_card_times',
                    'dip_inserting_times',
                    'nfc_card_reading_times',
                    'front_camera_open_times',
                    'rear_camera_open_times',
                    'charge_times',
                    'total_memory',
                    'available_memory',
                    'total_flash_memory',
                    'available_flash_memory',
                    'total_mobile_data',
                    'total_boot_time',
                    'total_length_printed',
                    'installed_apps'
        
                    )->chunk($chunk, function ($results) use ($output, $chunk) {
                    
                    foreach ($results as $row) {
                        
                        
                        //$no = session()->get('nodiag');
                        $no = intval(Redis::command('get', ['nodiag'])); 
                        if($no==$chunk)
                        {
                            //$c = session()->get('ckdiag') + $no;
                            $c = intval(Redis::command('get', ['ckdiag'])) + $no;
                            //session(['ckdiag' => $c]);
                           
                            Redis::set('ckdiag',  $c);
                           
                        }
                       
                        fputcsv($output, 
                        [
                        $no,
                        $row->terminal_ext_id, 
                        $row->terminal_id, 
                        $row->terminal_sn,
                        $row->battery_temp,
                        $row->battery_percentage,
                        $row->latitude,
                        $row->longitude,
                        $row->meid,
                        $row->switching_times,
                        $row->swiping_card_times,
                        $row->dip_inserting_times,
                        $row->nfc_card_reading_times,
                        $row->front_camera_open_times,
                        $row->rear_camera_open_times,
                        $row->charge_times,
                        $row->total_memory,
                        $row->available_memory,
                        $row->total_flash_memory,
                        $row->available_flash_memory,
                        $row->total_mobile_data,
                        $row->total_boot_time,
                        $row->total_length_printed,
                        $row->installed_apps
                       ],
                        "\t",'"');
                       
                        $no = $no + 1;
                        //session(['nodiag' => $no]);
                       
                        Redis::set('nodiag', $no);
                       
                    }
                });
                
                fclose($output);
           
            // ==== end batas ==

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    */
    
    public function handle2()
    {
        try {
           
            //====== batas == 
            $filename = $this->fileName; // time()."-data-report-diagnotics.xls";
            $output=fopen(base_path().'/storage/app/'.$filename,'w+');
    
          
            fputcsv($output, [
                'No',
                'Terminal Ext Id', 
                'Terminal Id',
                'Terminal SN',
                'Battery Temp',
                'Battery Percentage',
                'Latitude',
                'Longitude',
                'Meid',
                'Switching times',
                'Swiping card times',
                'Dip Inserting Times',
                'Nfc Card reading times',
                'Front camera open times',
                'Rear Camera Open times',
                'Charge times',
                'Total memory',
                'Available memory',
                'Total Flash Memory',
                'Available Flash Memory',
                'Total Mobile data',
                'Total Boot time',
                'Total Length printed',
                'Installed Apps'
                ],
                "\t",'"');
                $chunk = 50000;
                $results = DiagnosticReport::query()->select(
                    'terminal_ext_id',
                    'terminal_id', 
                    'terminal_sn',
                    'battery_temp',
                    'battery_percentage',
                    'latitude',
                    'longitude',
                    'meid',
                    'switching_times',
                    'swiping_card_times',
                    'dip_inserting_times',
                    'nfc_card_reading_times',
                    'front_camera_open_times',
                    'rear_camera_open_times',
                    'charge_times',
                    'total_memory',
                    'available_memory',
                    'total_flash_memory',
                    'available_flash_memory',
                    'total_mobile_data',
                    'total_boot_time',
                    'total_length_printed',
                    'installed_apps'
        
                    )->get();
                    
                    foreach ($results as $row) 
                    {
                        
                        
                        //$no = session()->get('nodiag');
                        $no = intval(Redis::command('get', ['nodiag'])); 
                        
                       
                        fputcsv($output, 
                        [
                        $no,
                        $row->terminal_ext_id, 
                        $row->terminal_id, 
                        $row->terminal_sn,
                        $row->battery_temp,
                        $row->battery_percentage,
                        $row->latitude,
                        $row->longitude,
                        $row->meid,
                        $row->switching_times,
                        $row->swiping_card_times,
                        $row->dip_inserting_times,
                        $row->nfc_card_reading_times,
                        $row->front_camera_open_times,
                        $row->rear_camera_open_times,
                        $row->charge_times,
                        $row->total_memory,
                        $row->available_memory,
                        $row->total_flash_memory,
                        $row->available_flash_memory,
                        $row->total_mobile_data,
                        $row->total_boot_time,
                        $row->total_length_printed,
                        $row->installed_apps
                       ],
                        "\t",'"');
                       
                        $no = $no + 1;
                        //session(['nodiag' => $no]);
                       
                        Redis::set('nodiag', $no);
                       
                    }
                //});
                
                fclose($output);
           
            // ==== end batas ==

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }


    public function handle()
    {
        $filename = $this->fileName; // time()."-data-report-diagnotics.xls";
        $filename = base_path().'/storage/app/'.$filename;
        
        
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        
        $sheet->setCellValue('A1', 'No');
        $sheet->setCellValue('B1', 'Terminal Ext Id');
        $sheet->setCellValue('C1', 'Terminal Id');
        $sheet->setCellValue('D1', 'Terminal SN');
        $sheet->setCellValue('E1', 'Battery Temp');
        $sheet->setCellValue('F1', 'Battery Percentage');

        $results = DiagnosticReport::query()->select(
            'terminal_ext_id',
            'terminal_id', 
            'terminal_sn',
            'battery_temp',
            'battery_percentage',
            'latitude',
            'longitude',
            'meid',
            'switching_times',
            'swiping_card_times',
            'dip_inserting_times',
            'nfc_card_reading_times',
            'front_camera_open_times',
            'rear_camera_open_times',
            'charge_times',
            'total_memory',
            'available_memory',
            'total_flash_memory',
            'available_flash_memory',
            'total_mobile_data',
            'total_boot_time',
            'total_length_printed',
            'installed_apps'
            )->get();

        $i = 2;
        $no = 1;
        foreach ($results as $d) 
        {
            Redis::set('nodiag', $no);
            $sheet->setCellValue('A'.$i, $no++);
            $sheet->setCellValue('B'.$i, $d->terminal_ext_id);
            $sheet->setCellValue('C'.$i, $d->terminal_id);
            $sheet->setCellValue('D'.$i, $d->terminal_sn);
            $sheet->setCellValue('E'.$i, $d->battery_temp);    
            $sheet->setCellValue('F'.$i, $d->battery_percentage);    
            $i++;
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save($filename); 
    }
    

   
   
}
