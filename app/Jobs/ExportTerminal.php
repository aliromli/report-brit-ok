<?php

namespace App\Jobs;

use Illuminate\Support\Facades\DB;
use App\Models\TerminalReport;
use Exception;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;


class ExportTerminal implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $fileName;
    protected $process;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($process, $fileName)
    {
        $this->fileName = $fileName;
        $this->process = $process;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    /*
    public function handle()
    {
        try {
           
            //====== batas == 
            $filename = $this->fileName; // time()."-data-report-diagnotics.xls";
            $output=fopen(base_path().'/storage/app/'.$filename,'w+');
    
            //$id = explode('-',$filenam)[0];
           
            fputcsv($output, [
                    'No',
                    'SN',
                    'Imei',
                    'Terminal_id',
                    'Merchant_id',
                    'Merchant_name1',
                    'Merchant_name2',
                    'Merchant_name3',
                    'Feature_sale',
                    'Feature_sale_tip',
                    'Feature_sale_redemption',
                    'Feature_card_verification',
                    'Feature_sale_completion',
                    'Feature_installment',
                    'Feature_sale_fare_non_fare',
                    'Feature_manual_key_in',
                    'Feature_qris',
                    'Feature_contactless',
                    'Random_pin_keypad',
                    'Beep_pin_keypad',
                    'Auto_logon',
                    'Next_logon',
                    'Installment1_options',
                    'Installment2_options',
                    'Installment3_options',
                    'State',
                    'App_version',
                    'Launcher_version',
                    'Vfs_version',
                    'fss_version',
                    //'ECR_version',
                    //'ECR_version',
                    //'ROM_version',
                    //'security_patch_version',
                    //'update_ts',
                    'last_diagnostic_time',
                    'last_heartbeat_time',
                    'latitude',
                    'longitude',
                    //'connection',
                    //'wifi_strength',
                    'cell_name',
                    'cell_type',
                    'cell_strength',
                    'Push Logon',
                    'Host Report',
                    'Host Logging',
                    'Rom Version',
                    'Sp Version'
                
                ],
                "\t",'"');
                $chunk = 1000;
                TerminalReport::select(
                    'sn',
                    'imei',
                    'terminal_id',
                    'merchant_id',
                    'merchant_name1',
                    'merchant_name2',
                    'merchant_name3',
                    'feature_sale',
                    'feature_sale_tip',
                    'feature_sale_redemption',
                    'feature_card_verification',
                    'feature_sale_completion',
                    'feature_installment',
                    'feature_sale_fare_non_fare',
                    'feature_manual_key_in',
                    'feature_qris',
                    'feature_contactless',
                    'random_pin_keypad',
                    'beep_pin_keypad',
                    'auto_logon',
                    'next_logon',
                    'installment1_options',
                    'installment2_options',
                    'installment3_options',
                    'state',
                    'app_version',
                    'launcher_version',
                    'vfs_version',
                    'vfss_version',
                    //'ECR_version',
                    //'ECR_version',
                    //'ROM_version',
                    //'security_patch_version'=>'',
                    //'update_ts'=>'',
                    'last_diagnostic_time',
                    'last_heartbeat_time',
                    'latitude',
                    'longitude',
                    //'connection'=>'',	
                    //'wifi_strength'	=>'',
                    'cell_name',
                    'cell_type',
                    'cell_strength',
                    'push_logon',
                    'host_report',
                    'host_logging',
                    'rom_version',
                    'sp_version',
                    
        
                    )->chunkById($chunk, function ($results) use ($output, $chunk) {
                    
                    foreach ($results as $row) {
                        
                        
                        //$no = session()->get('nodiag');
                        $no = intval(Redis::command('get', ['noterm'])); 
                        if($no==$chunk)
                        {
                            //$c = session()->get('ckdiag') + $no;
                            $c = intval(Redis::command('get', ['ckterm'])) + $no;
                            //session(['ckdiag' => $c]);
                           
                            Redis::set('ckterm',  $c);
                           
                        }
                       
                        fputcsv($output, 
                        [
                        $no,
                        $row->sn, 
                      	$row->imei,
                        $row->terminal_id,
						$row->merchant_id,
						$row->merchant_name1,
						$row->merchant_name2,
						$row->merchant_name3,
						$this->bol($row->feature_sale),
						$this->bol($row->feature_sale_tip),
						$this->bol($row->feature_sale_redemption),
						$this->bol($row->feature_card_verification),
						$this->bol($row->feature_sale_completion),
						$this->bol($row->feature_installment),
						$this->bol($row->feature_sale_fare_non_fare),
						$this->bol($row->feature_manual_key_in),
						$this->bol($row->feature_qris),
						$this->bol($row->feature_contactless),

						$row->random_pin_keypad,
						$row->beep_pin_keypad,
						$this->bol($row->auto_logon),
						$row->next_logon,
						$row->installment1_options,
						$row->installment2_options,
						$row->installment3_options,
						$row->state,
						$row->app_version,
						$row->launcher_version,
						$row->vfs_version,
						$row->vfss_version,
						//'ECR_version'=>'',
						//'ECR_version'=>'',
						//'ROM_version'=>'',
						//'security_patch_version'=>'',
						//'update_ts'=>'',
						$row->last_diagnostic_time,
						$row->last_heartbeat_time,
						$row->latitude,
						$row->longitude,
						//'connection'=>'',	
						//'wifi_strength'	=>'',
						$row->cell_name,
						$row->cell_type,
						$row->cell_strength,
						$row->push_logon,
						$this->bol($row->host_report),
						$this->bol($row->host_logging),
						$row->rom_version,
						$row->sp_version

                       
                       ],
                        "\t",'"');
                       
                        $no = $no + 1;
                         
                        Redis::set('noterm', $no);
                       
                    }
                });
                
                fclose($output);
                

            // ==== end batas ==

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    */
    public function handle()
    {
        try {
           
            //====== batas == 
            $filename = $this->fileName; // time()."-data-report-diagnotics.xls";
            $output=fopen(base_path().'/storage/app/'.$filename,'w+');
    
            //$id = explode('-',$filenam)[0];
           
            fputcsv($output, [
                    'No',
                    'SN',
                    'Imei',
                    'Terminal_id',
                    'Merchant_id',
                    'Merchant_name1',
                    'Merchant_name2',
                    'Merchant_name3',
                    'Feature_sale',
                    'Feature_sale_tip',
                    'Feature_sale_redemption',
                    'Feature_card_verification',
                    'Feature_sale_completion',
                    'Feature_installment',
                    'Feature_sale_fare_non_fare',
                    'Feature_manual_key_in',
                    'Feature_qris',
                    'Feature_contactless',
                    'Random_pin_keypad',
                    'Beep_pin_keypad',
                    'Auto_logon',
                    'Next_logon',
                    'Installment1_options',
                    'Installment2_options',
                    'Installment3_options',
                    'State',
                    'App_version',
                    'Launcher_version',
                    'Vfs_version',
                    'fss_version',
                    //'ECR_version',
                    //'ECR_version',
                    //'ROM_version',
                    //'security_patch_version',
                    //'update_ts',
                    'last_diagnostic_time',
                    'last_heartbeat_time',
                    'latitude',
                    'longitude',
                    //'connection',
                    //'wifi_strength',
                    'cell_name',
                    'cell_type',
                    'cell_strength',
                    'Push Logon',
                    'Host Report',
                    'Host Logging',
                    'Rom Version',
                    'Sp Version'
                
                ],
                "\t",'"');
                //$chunk = 1000;
                $results = TerminalReport::query()->select(
                    'sn',
                    'imei',
                    'terminal_id',
                    'merchant_id',
                    'merchant_name1',
                    'merchant_name2',
                    'merchant_name3',
                    'feature_sale',
                    'feature_sale_tip',
                    'feature_sale_redemption',
                    'feature_card_verification',
                    'feature_sale_completion',
                    'feature_installment',
                    'feature_sale_fare_non_fare',
                    'feature_manual_key_in',
                    'feature_qris',
                    'feature_contactless',
                    'random_pin_keypad',
                    'beep_pin_keypad',
                    'auto_logon',
                    'next_logon',
                    'installment1_options',
                    'installment2_options',
                    'installment3_options',
                    'state',
                    'app_version',
                    'launcher_version',
                    'vfs_version',
                    'vfss_version',
                    //'ECR_version',
                    //'ECR_version',
                    //'ROM_version',
                    //'security_patch_version'=>'',
                    //'update_ts'=>'',
                    'last_diagnostic_time',
                    'last_heartbeat_time',
                    'latitude',
                    'longitude',
                    //'connection'=>'',	
                    //'wifi_strength'	=>'',
                    'cell_name',
                    'cell_type',
                    'cell_strength',
                    'push_logon',
                    'host_report',
                    'host_logging',
                    'rom_version',
                    'sp_version'
                    
        
                    )->get();
                    
                    foreach ($results as $row) {
                        
                        
                        //$no = session()->get('nodiag');
                        $no = intval(Redis::command('get', ['noterm'])); 
                        // if($no==$chunk)
                        // {
                        //     //$c = session()->get('ckdiag') + $no;
                        //     $c = intval(Redis::command('get', ['ckterm'])) + $no;
                        //     //session(['ckdiag' => $c]);
                           
                        //     Redis::set('ckterm',  $c);
                           
                        // }
                       
                        fputcsv($output, 
                        [
                        $no,
                        $row->sn, 
                      	$row->imei,
                        $row->terminal_id,
						$row->merchant_id,
						$row->merchant_name1,
						$row->merchant_name2,
						$row->merchant_name3,
						$this->bol($row->feature_sale),
						$this->bol($row->feature_sale_tip),
						$this->bol($row->feature_sale_redemption),
						$this->bol($row->feature_card_verification),
						$this->bol($row->feature_sale_completion),
						$this->bol($row->feature_installment),
						$this->bol($row->feature_sale_fare_non_fare),
						$this->bol($row->feature_manual_key_in),
						$this->bol($row->feature_qris),
						$this->bol($row->feature_contactless),

						$row->random_pin_keypad,
						$row->beep_pin_keypad,
						$this->bol($row->auto_logon),
						$row->next_logon,
						$row->installment1_options,
						$row->installment2_options,
						$row->installment3_options,
						$row->state,
						$row->app_version,
						$row->launcher_version,
						$row->vfs_version,
						$row->vfss_version,
						//'ECR_version'=>'',
						//'ECR_version'=>'',
						//'ROM_version'=>'',
						//'security_patch_version'=>'',
						//'update_ts'=>'',
						$row->last_diagnostic_time,
						$row->last_heartbeat_time,
						$row->latitude,
						$row->longitude,
						//'connection'=>'',	
						//'wifi_strength'	=>'',
						$row->cell_name,
						$row->cell_type,
						$row->cell_strength,
						$row->push_logon,
						$this->bol($row->host_report),
						$this->bol($row->host_logging),
						$row->rom_version,
						$row->sp_version

                       
                       ],
                        "\t",'"');
                       
                        $no = $no + 1;
                         
                        Redis::set('noterm', $no);
                       
                    }
                //});
                
                fclose($output);
                

            // ==== end batas ==

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    public function bol($data)
	{
      if(isset($data))
	  {
		 return ($data==TRUE) ? "YES" : "NO";
	  }
	  else
	  {
		return "";
	  }
	}
}
