<?php

namespace App\Jobs;

use Illuminate\Support\Facades\DB;
use App\Models\DiagnosticReport;
use App\Models\Diagnostic;

use Exception;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem;



class ExportDiag2 implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $fileName;
    protected $process;


    
    public function __construct($process, $fileName)
    {
        $this->fileName = $fileName;
        $this->process = $process;
    }

  
    public function handle(){

        
        $file = new Filesystem;
        $file->cleanDirectory(base_path().'/storage/app/diagnostic');
    
        
        
        $ldate = date('Y-m-d H:i:s');
        $ldate = str_replace(" ",".",$ldate);
        $ldate = str_replace(":",".",$ldate);
        $filename = time()."-data-report-diagnotics-waktu-".$ldate.".xls";
        $output=fopen(base_path().'/storage/app/diagnostic/'.$filename,'w+');

        session(['nodiag' => 1]);
        //session(['totaldiag' =>Diagnostic::count()]); //DiagnosticReport::count()
        session(['ckdiag' => 1]);
        fputcsv($output, [
            'No',
            'Terminal Ext Id', 
            'Terminal Id',
            'Terminal SN',
            'Battery Temp',
            'Battery Percentage',
            'Latitude',
            'Longitude',
            'Meid',
            'Switching times',
            'Swiping card times',
            'Dip Inserting Times',
            'Nfc Card reading times',
            'Front camera open times',
            'Rear Camera Open times',
            'Charge times',
            'Total memory',
            'Available memory',
            'Total Flash Memory',
            'Available Flash Memory',
            'Total Mobile data',
            'Total Boot time',
            'Total Length printed',
            'Installed Apps'
            ],
            "\t",'"');
            $chunk = 50000;
            DiagnosticReport::select(
                'terminal_ext_id',
                'terminal_id', 
                'terminal_sn',
                'battery_temp',
                'battery_percentage',
                'latitude',
                'longitude',
                'meid',
                'switching_times',
                'swiping_card_times',
                'dip_inserting_times',
                'nfc_card_reading_times',
                'front_camera_open_times',
                'rear_camera_open_times',
                'charge_times',
                'total_memory',
                'available_memory',
                'total_flash_memory',
                'available_flash_memory',
                'total_mobile_data',
                'total_boot_time',
                'total_length_printed',
                'installed_apps'
    
                )->chunk($chunk, function ($results) use ($output, $chunk) {
                
                foreach ($results as $row) {
                    
                    
                    $no = session()->get('nodiag');
                    if($no==$chunk)
                    {
                        $c = session()->get('ckdiag') + $no;
                        session(['ckdiag' => $c]);
                       
                    }
                   
                    fputcsv($output, 
                    [
                    $no,
                    $row->terminal_ext_id, 
                    $row->terminal_id, 
                    $row->terminal_sn,
                    $row->battery_temp,
                    $row->battery_percentage,
                    $row->latitude,
                    $row->longitude,
                    $row->meid,
                    $row->switching_times,
                    $row->swiping_card_times,
                    $row->dip_inserting_times,
                    $row->nfc_card_reading_times,
                    $row->front_camera_open_times,
                    $row->rear_camera_open_times,
                    $row->charge_times,
                    $row->total_memory,
                    $row->available_memory,
                    $row->total_flash_memory,
                    $row->available_flash_memory,
                    $row->total_mobile_data,
                    $row->total_boot_time,
                    $row->total_length_printe,
                    $row->installed_apps
                   ],
                    "\t",'"');
                   
                    $no = $no + 1;
                    session(['nodiag' => $no]);
                   
                }
            });
            
            fclose($output);
            
            session()->forget('nodiag');
            //session()->forget('totaldiag');
            session()->forget('ckdiag');
        
    }
   
   
}
