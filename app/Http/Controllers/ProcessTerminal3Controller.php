<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Illuminate\Support\Facades\Redis;

class ProcessTerminal3Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    
    /**
     * Check or update process
     * with json return
     * */
    public function updateProcessExport(Request $request) {

        //--
        $response = new StreamedResponse();
        $response->headers->set('Content-Type', 'text/event-stream');
        $response->headers->set('Cache-Control', 'no-cache');
        $response->headers->set('Connection', 'keep-alive');
        $response->headers->set('X-Accel-Buffering', 'no');
        $data = $request->ck;//session()->get('ckdiag');

        $response->setCallback(function() use ($data) {
            
            $proses = intval(Redis::command('get', ['noterm'])); 
            $total =  intval(Redis::command('get', ['totalterm']));  //session()->get('totaldiag');
            if($total==0)
            {
               $percentage = 0;
               echo "data: " . $percentage . "\n\n";
            }
            else
            {
               $percentage = round(($proses / $total) * 100);
               echo "data: " . $percentage . "\n\n";
               
            }
            ob_flush();
            flush();
        });
        
           

        $response->send();

    }
}
