<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\DiagnosticReport;
use App\Models\Diagnostic;
use App\Models\DiagnosticInfo;

use App\Exports\DiagnosticExport;
use App\Exports\DiagnosticCollectionExport;

use Maatwebsite\Excel\Facades\Excel;
use DateTime;
use Illuminate\Support\Str;
use Spatie\SimpleExcel\SimpleExcelWriter;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Illuminate\Support\Facades\File;
use App\Jobs\ExportDiag;
use Illuminate\Support\Facades\Redis;
//use Carbon;

class Terminal3Controller extends Controller
{
    
    // public function index(Request $request)
    // {
      
    //     $data['title'] = 'Terminal Report';
    //     $data['proses'] = Redis::command('get', ['noterm']); 
    //     $data['tes'] = Redis::command('get', ['noterm']);
    //     return view('pages.terminal3.index', $data);
    // }

    public function reportExcel()
    {
        $data['title'] = 'Terminal Report';
        $path = storage_path('app'.DIRECTORY_SEPARATOR.'terminal'.DIRECTORY_SEPARATOR);
        $files = File::allFiles($path);

        $allMedia = [];
        foreach ($files as $path) {
            $files = pathinfo($path);
            $allMedia[] = $files['basename'];
        }
       
        $data['files'] = $allMedia;
        return view('pages.terminal3.readExcel', $data);

    }

    public function download(Request $request)  
    {
        $fileName = $request->fileName;
        return response()->download(base_path().'/storage/app/terminal/'.$fileName);
    }

    // public function detail(Request $request)
    // {
    //     $data['title'] = 'Terminal  Report'; 
    //     $data['det'] = DiagnosticReport::where('id',$request->id)->first();
    //     return view('pages.terminal3.detail', $data);
    // }
    // public function export(){
        
    //     Redis::set('totalterm',  TerminalReport::count());
    //     Redis::set('ckterm',  1);
    //     Redis::set('noterm',  1);

    //     $porses = Redis::command('get', ['ckterm']); //session()->get('ckdiag');
    //     $fileName =  time()."-data-report-diagnotics.xls";
    //     Redis::set('filediag',  $fileName);
    //     ExportDiag::dispatch($porses,$fileName)->delay(now()->addSeconds(3));

    //     return response()->json([
    //         'responseCode' => 200,
    //         'responseMessage' => 'Export',
    //         'nodiag' =>  intval(Redis::command('get', ['nodiag'])),
    //         'totaldiag' => intval(Redis::command('get', ['totaldiag'])),
    //         'ckdiag' => intval($porses), //session()->get('ckdiag'),
    //         'fileName' =>  $fileName
            
    //     ]); 

    // }

    // public function exportxx(Request $request){
    //     $begin = memory_get_usage();
    //     $handle = fopen('export.csv', 'w');
    
    //     DiagnosticReport::chunk(1000, function ($persons) use ($handle) {
    //         foreach ($persons as $row) {
    //             fputcsv($handle, $row->toArray(), ';');
    //         }
    //     });
    
    //     fclose($handle);
    //     $end = (memory_get_usage()-$begin);
    //     echo "Memory Usage: " . ($end/1048576) . " MB \n";
       
    // }

    // public function export000()  
    // {
    //     return Excel::download(new DiagnosticCollectionExport, 'persibxx.xlsx');
        
    // }
   
    // public function updateProcessExport2(Request $request)
    // {
    //          $proses = session()->get('ckdiag');
    //          $total =  100000;//session()->get('totaldiag');
    //          //echo $proses;
    //          //echo $total; round((14000/100000)*100);
    //          if($total==0)
    //          {
    //             $percentage = 0;
    //             //echo "data: " . $percentage . "\n\n";
    //             return response()->json([
    //                 'responseCode' => 200,
    //                 'responseMessage' => 'Uploaded',
    //                 'data' => $percentage
    //             ]); 
    //          }
    //          else
    //          {
    //             $percentage = round(($proses / $total) * 100);
    //             return response()->json([
    //                 'responseCode' => 200,
    //                 'responseMessage' => 'Uploaded',
    //                 'data' => $percentage
    //             ]); 
    //          }
    // }

    // public function updateProcessExport(Request $request)
    // {
        
    //      $response = new StreamedResponse();
    //      $response->headers->set('Content-Type', 'text/event-stream');
    //      $response->headers->set('Cache-Control', 'no-cache');
    //      $response->headers->set('Connection', 'keep-alive');
    //      $response->headers->set('X-Accel-Buffering', 'no');
    //      $data = $request->ck;

    //      $response->setCallback(function() use ($data) {
    //          $proses = intval(Redis::command('get', ['nodiag'])); 
    //          $total =  intval(Redis::command('get', ['totaldiag']));  
             
    //          if($total==0)
    //          {
    //             $percentage = 0;
    //             echo "data: " . $percentage . "\n\n";
    //          }
    //          else
    //          {
    //             $percentage = round(($proses / $total) * 100);
    //             echo "data: " . $percentage . "\n\n";
                
    //          }
    //          ob_flush();
    //          flush();
    //      });
         
            

    //      $response->send();
    // }

    // public function deleteFileXls(Request $request) {
        
      
    //     Redis::del('nodiag');
    //     Redis::del('totaldiag');
    //     Redis::del('ckdiag'); 
    //     Redis::del('filediag'); 
        


    //     unlink(base_path().'/storage/app/'.$request->fileName);
    //     return response()->json([
    //         'responseCode' => 200,
    //         'responseMessage' => 'Deleted'
    //     ]); 

    // }

    // public function export_xxxa($processId=1) {

    //     $response = new StreamedResponse();
    //     $response->headers->set('Content-Type', 'text/event-stream');
    //     $response->headers->set('Cache-Control', 'no-cache');
    //     $response->headers->set('Connection', 'keep-alive');
    //     $response->headers->set('X-Accel-Buffering', 'no');

    //     $response->setCallback(function() use ($processId) {
    //         //$process = Process::find($processId);
    //         $percentage =2;// round(($process->processed / $process->total) * 100);
    //         echo "data: " . $percentage . "\n\n";
    //         ob_flush();
    //         flush();
    //     });

    //     $response->send();

    // }

   

    // public function downloadOnly(Request $request)  
    // {
    //     $fileName = Redis::command('get', ['filediag']);
    //     return response()->download(base_path().'/storage/app/'.$fileName);
    // }
    // public function download(Request $request)  
    // {
    //     $fileName = $request->fileName;
    //     return response()->download(base_path().'/storage/app/diagnostic/'.$fileName);
    // }
 

   

    // public function exportDiagSch()
    // {
    //     Redis::set('totaldiag',  DiagnosticReport::count());
    //     Redis::set('ckdiag',  1);
    //     Redis::set('nodiag',  1);

    //     $porses = Redis::command('get', ['ckdiag']); 
    //     $fileName =  time()."-data-report-diagnotics.xls";
    //     Redis::set('filediag',  $fileName);
    //     ExportDiag::dispatch($porses,$fileName)->delay(now()->addSeconds(3));
    // }
    // public function reportExcel()
    // {
    //     $data['title'] = 'Diagnostic Report';
        
        
    //     //$path = public_path();
    //     $path = storage_path('app'.DIRECTORY_SEPARATOR.'diagnostic'.DIRECTORY_SEPARATOR);
    //     $files = File::allFiles($path);

       
    //     $allMedia = [];


    //     foreach ($files as $path) {
    //         $files = pathinfo($path);
    //         $allMedia[] = $files['basename'];
    //     }
      
    //     $data['files'] = $allMedia;
    //     return view('pages.diagnostic3.readExcel', $data);

    // }
    
    

    //public function datatablesExcel()
    //{
        //$file = base_path().'/storage/app/diagnostic/'.$filename,'w+'
    //}

    // public function handleReadExcel()
    // {
    //     try {
    //         $filename = 'tes2.xlsx';
    //         $path = storage_path('app'.DIRECTORY_SEPARATOR.'diagnostic'.DIRECTORY_SEPARATOR.$filename);
    //         $reader = ReaderEntityFactory::createReaderFromFile($path);
    //         $reader->open($path);

    //         foreach ($reader->getSheetIterator() as $sheet) {
    //             if($sheet->getIndex() === 0) {
                    
    //                 $this->readSheet($sheet);
    //                 break;
    //             }
    //         }

    //         $reader->close();


    //     } catch (Exception $e) {
    //         echo $e->getMessage();
    //     }
    // }
    // private function readSheet($sheet)
    // {
    //     foreach ($sheet->getRowIterator() as $i => $row)
	// 	{

    //         if($i == 1) {
    //             continue;
    //         }

    //         if($i == 1001) {
    //             break;
    //         }
    //         $r = $row->toArray();

    //         echo $r[1]."-".$r[1]; echo '<br/>';
    //     }
    // }
}
