<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\DiagnosticReport;
use App\Models\Diagnostic;

use App\Exports\DiagnosticExport;
use App\Exports\DiagnosticCollectionExport;

use Maatwebsite\Excel\Facades\Excel;
use DateTime;
use Illuminate\Support\Str;
use Spatie\SimpleExcel\SimpleExcelWriter;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

class DiagnosticController extends Controller
{
    
    public function index(Request $request)
    {
      
        $data['title'] = 'Diagnostic Report';
        return view('pages.diagnostic.index', $data);
    }
    public function detail(Request $request)
    {
        $data['title'] = 'Diagnostic Detail Report'; 
        $data['det'] = DiagnosticReport::where('id',$request->id)->first();
        return view('pages.diagnostic.detail', $data);
    }

    // public function export($filename = 'users.xlsx') {
    //     init_set("max_execution_time",3600);
    //     try {
     
    //         $filename = !empty(request()->query('filename'))
    //             ? request()->query('filename') . '.xlsx'
    //             : $filename;
    //         // Adding uuid to filename avoid conflict can achieve the same result by adding timestamp
    //         $filename = Str::uuid()->toString() . '_' . $filename;
    //         (new InvoicesExport(""))->store($filename, 'local', \Maatwebsite\Excel\Excel::XLSX, [
    //             'lazy' => true,
    //             'chunck_size' => 3000,
    //         ]);
    //         return response()->download(storage_path('app/' . $filename))
    //             ->deleteFileAfterSend(true);
    //     } catch (\Exception $e) {
    //         return $this->badRequest($e);
    //     }
    // }

    // public function export(Request $request) {
    //     $query = DB::select(DB::raw("select * from diagnostics"));
    //     $query = collect($query);
    
    //     $writer = SimpleExcelWriter::streamDownload('your-export.XLSX');
    //     foreach (range(0, $query->count()) as $i) {
    //         $writer->addRow([
    //             'ID' => $query[$i]->id,
    //             'terminal_sn' => $query[$i]->terminal_sn
    //         ]);
    
    //         if ($i % $query->count() === 0) {
    //             flush(); // Flush the buffer
    //         }
    //     }
    
    //     $writer->toBrowser();
    // }

    public function exportxx(Request $request){
        $begin = memory_get_usage();
        $handle = fopen('export.csv', 'w');
    
        DiagnosticReport::chunk(1000, function ($persons) use ($handle) {
            foreach ($persons as $row) {
                fputcsv($handle, $row->toArray(), ';');
            }
        });
    
        fclose($handle);
        $end = (memory_get_usage()-$begin);
        echo "Memory Usage: " . ($end/1048576) . " MB \n";
        
        /*$begin = memory_get_usage();
        $data = DiagnosticReport::all();
        $handle = fopen('export.csv', 'w');
        foreach ($data as $row) {
            fputcsv($handle, $row->toArray(), ';');
        }
        fclose($handle);
        $end = (memory_get_usage()-$begin);
        echo "Memory Usage: " . ($end/1048576) . " MB \n";*/
    }

    public function export000()  
    {
        return Excel::download(new DiagnosticCollectionExport, 'persibxx.xlsx');
        
    }

    public function export($processId=1){

        // $FolderToDelete = base_path('storage/app');
        // $fs = new \Illuminate\Filesystem\Filesystem;
        // $fs->cleanDirectory($FolderToDelete);   

        $filename = time()."-data-report-diagnotics.xls";
        $output=fopen(base_path().'/storage/app/'.$filename,'w+');

        session(['nodiag' => 1]);
        session(['totaldiag' =>100000]); //DiagnosticReport::count()
        session(['ckdiag' => 1]);
        fputcsv($output, [
            'No',
            'Terminal Ext Id', 
            'Terminal Id',
            'Terminal SN',
            'Battery Temp',
            'Battery Percentage',
            'Latitude',
            'Longitude',
            'Meid',
            'Switching times',
            'Swiping card times',
            'Dip Inserting Times',
            'Nfc Card reading times',
            'Front camera open times',
            'Rear Camera Open times',
            'Charge times',
            'Total memory',
            'Available memory',
            'Total Flash Memory',
            'Available Flash Memory',
            'Total Mobile data',
            'Total Boot time',
            'Total Length printed',
            'Installed Apps'
            ],
            "\t",'"');
            $chunk = 1000;
            DiagnosticReport::select(
                'terminal_ext_id',
                'terminal_id', 
                'terminal_sn',
                'battery_temp',
                'battery_percentage',
                'latitude',
                'longitude',
                'meid',
                'switching_times',
                'swiping_card_times',
                'dip_inserting_times',
                'nfc_card_reading_times',
                'front_camera_open_times',
                'rear_camera_open_times',
                'charge_times',
                'total_memory',
                'available_memory',
                'total_flash_memory',
                'available_flash_memory',
                'total_mobile_data',
                'total_boot_time',
                'total_length_printed',
                'installed_apps'
    
                )->chunk($chunk, function ($results) use ($output, $chunk) {
                
                foreach ($results as $row) {
                    
                    
                    $no = session()->get('nodiag');
                    if($no==$chunk)
                    {
                        $c = session()->get('ckdiag') + $no;
                        session(['ckdiag' => $c]);
                       
                    }
                   
                    fputcsv($output, 
                    [
                    $no,
                    $row->terminal_ext_id, 
                    $row->terminal_id, 
                    $row->terminal_sn,
                    $row->battery_temp,
                    $row->battery_percentage,
                    $row->latitude,
                    $row->longitude,
                    $row->meid,
                    $row->switching_times,
                    $row->swiping_card_times,
                    $row->dip_inserting_times,
                    $row->nfc_card_reading_times,
                    $row->front_camera_open_times,
                    $row->rear_camera_open_times,
                    $row->charge_times,
                    $row->total_memory,
                    $row->available_memory,
                    $row->total_flash_memory,
                    $row->available_flash_memory,
                    $row->total_mobile_data,
                    $row->total_boot_time,
                    $row->total_length_printe,
                    $row->installed_apps
                   ],
                    "\t",'"');
                   
                    $no = $no + 1;
                    session(['nodiag' => $no]);

                    
                 
                   
                }
            });
            
            fclose($output);
            
            session()->forget('nodiag');
            session()->forget('totaldiag');
            session()->forget('ckdiag');
        
        

            return response()->download(base_path().'/storage/app/'.$filename);

    }

    public function updateProcessExport2(Request $request)
    {
             $proses = session()->get('ckdiag');
             $total =  100000;//session()->get('totaldiag');
             //echo $proses;
             //echo $total; round((14000/100000)*100);
             if($total==0)
             {
                $percentage = 0;
                //echo "data: " . $percentage . "\n\n";
                return response()->json([
                    'responseCode' => 200,
                    'responseMessage' => 'Uploaded',
                    'data' => $percentage
                ]); 
             }
             else
             {
                $percentage = round(($proses / $total) * 100);
                return response()->json([
                    'responseCode' => 200,
                    'responseMessage' => 'Uploaded',
                    'data' => $percentage
                ]); 
             }
    }
    public function updateProcessExport()
    {
         //--
         $response = new StreamedResponse();
         $response->headers->set('Content-Type', 'text/event-stream');
         $response->headers->set('Cache-Control', 'no-cache');
         $response->headers->set('Connection', 'keep-alive');
         $response->headers->set('X-Accel-Buffering', 'no');
         $data = session()->get('ckdiag');

         $response->setCallback(function() use ($data) {
             $proses = $data;
             $total =  100000;//session()->get('totaldiag');
             //echo $proses;
             //echo $total; round((14000/100000)*100);
             if($total==0)
             {
                $percentage = 0;
                echo "data: " . $percentage . "\n\n";
             }
             else
             {
                $percentage = 100;//round(($proses / $total) * 100);
                echo "data: " . $percentage . "\n\n";
             }
             ob_flush();
             flush();
         });
         
            

             $response->send();
    }
    public function deleteFileXls(Request $request) {

        unlink(base_path().'/storage/app/'.$request->fileName);
        return response()->json([
            'responseCode' => 200,
            'responseMessage' => 'Deleted'
        ]); 

    }

    public function export_xxxa($processId=1) {

        $response = new StreamedResponse();
        $response->headers->set('Content-Type', 'text/event-stream');
        $response->headers->set('Cache-Control', 'no-cache');
        $response->headers->set('Connection', 'keep-alive');
        $response->headers->set('X-Accel-Buffering', 'no');

        $response->setCallback(function() use ($processId) {
            //$process = Process::find($processId);
            $percentage =2;// round(($process->processed / $process->total) * 100);
            echo "data: " . $percentage . "\n\n";
            ob_flush();
            flush();
        });

        $response->send();

    }

    public function exportPPP()
    {
        
        $headers = [
            //'Content-type' => 'application/vnd.ms-excel',
            'Content-type' => 'application/json',
            'Content-Disposition' => 'attachment; filename=data.xls',
        ];

        // return response()->json([
        //     'responseCode' => 200,
        //     'responseMessage' => 'Exporting',
        //     'processId' => 1  text/plain    
        // ]); 

            // $data = response()->json([
            //     'responseCode' => 200,
            //     'responseMessage' => 'Exporting',
            //     'processId' => 1
            // ]);
        
        header('Content-type: application/json');
        header('Content-Disposition: tes2');

        $response = "TES";//new Response("TES");//Response("TES", 200, $headers);
        echo json_encode(["TES"=>"WERT"]);
    }

    public function downloadOnly()  
    {
        //return response()->download(base_path().'/storage/app/'.$filename);
    }

    public function export_back()  
    {
        //befor clean directory
        $FolderToDelete = base_path('storage/app');
        $fs = new \Illuminate\Filesystem\Filesystem;
        $fs->cleanDirectory($FolderToDelete);   
        
        
        // $headers = [
        //     'Content-type' => 'application/vnd.ms-excel',
        //     'Content-Disposition' => 'attachment; filename=data.xls',
        // ];
        /*
         'total_memory' => $data->total_memory,
						'available_memory' => $data->available_memory,
						'total_flash_memory' => $data->total_flash_memory,
						'available_flash_memory' => $data->available_flash_memory,
						'total_mobile_data' => $data->current_boot_time,
						'total_boot_time' => $data->total_boot_time,
						'total_length_printed' => $data->total_length_printed,
						'installed_apps' => $data->installed_apps

        */
        
        // Create the XLS file using PHP's built-in functions
        //$output = fopen('php://output', 'w');
        //$output = fopen("c:\\", "w");
        $filename = time()."-data-report-diagnotics.xls";
        $output=fopen(base_path().'/storage/app/'.$filename,'w+');
        //$output2=fopen(base_path().'/storage/app/temp.txt','w+');
        session(['key' => 1]);
        fputcsv($output, [
            'No',
            'Terminal Ext Id', 
            'Terminal Id',
            'Terminal SN',
            'Battery Temp',
            'Battery Percentage',
            'Latitude',
            'Longitude',
            'Meid',
            'Switching times',
            'Swiping card times',
            'Dip Inserting Times',
            'Nfc Card reading times',
            'Front camera open times',
            'Rear Camera Open times',
            'Charge times',
            'Total memory',
            'Available memory',
            'Total Flash Memory',
            'Available Flash Memory',
            'Total Mobile data',
            'Total Boot time',
            'Total Length printed',
            'Installed Apps'
            ],
            "\t",'"');
        
        // Use chunking to retrieve and process the data in smaller batches
        $no_tem = [];
        //$count = DiagnosticReport::count();
        DiagnosticReport::select(
            'terminal_ext_id',
            'terminal_id', 
            'terminal_sn',
            'battery_temp',
            'battery_percentage',
            'latitude',
            'longitude',
            'meid',
            'switching_times',
            'swiping_card_times',
            'dip_inserting_times',
            'nfc_card_reading_times',
            'front_camera_open_times',
            'rear_camera_open_times',
            'charge_times',
            'total_memory',
            'available_memory',
            'total_flash_memory',
            'available_flash_memory',
            'total_mobile_data',
            'total_boot_time',
            'total_length_printed',
            'installed_apps'

            )->chunk(1000, function ($results) use ($output) {
            //$no = ($count-$count)+1;
            //$no = 1;
           
           

            // if($value==100)
            // {
            //     $no = $no;
            // }

            // if($value>=100)
            // {
            //     $no = $value;
            // }

            //$myfile = fopen("webdictionary.txt", "r") or die("Unable to open file!");
           
            //fclose($myfile); strlen

            // if($no==100)
            // {
                
            //     $myfile = fopen(base_path().'/storage/app/temp.txt', "r");
            //     $fg =  fread($myfile,filesize(base_path().'/storage/app/temp.txt'));

            //     $no = strlen($fg);
            // }

            //$no = file_get_contents(base_path().'/storage/app/'.$filename);
            //$no = count($results);
            foreach ($results as $row) {
              
                $no = session()->get('key');
                fputcsv($output, 
                [
                $no,
                $row->terminal_ext_id, 
                $row->terminal_id, 
                $row->terminal_sn,
                $row->battery_temp,
                $row->battery_percentage,
                $row->latitude,
                $row->longitude,
                $row->meid,
                $row->switching_times,
                $row->swiping_card_times,
                $row->dip_inserting_times,
                $row->nfc_card_reading_times,
                $row->front_camera_open_times,
                $row->rear_camera_open_times,
                $row->charge_times,
                $row->total_memory,
                $row->available_memory,
                $row->total_flash_memory,
                $row->available_flash_memory,
                $row->total_mobile_data,
                $row->total_boot_time,
                $row->total_length_printe,
                $row->installed_apps
               ],
                "\t",'"');
                //$no_tem++;// = $no + $no_tem;
                $no = $no + 1;
                session(['key' => $no]);
                //$no++;
               // $no_temp[$no] = $no;
                //array_merge()
               
            }
        });
        
        fclose($output);
        //fclose($output2);
        session()->forget('key');
        
        // Create a Response instance with the file's content and headers
        //$response = new Response(file_get_contents('php://output'), 200, $headers);
        //$response = new Response(file_get_contents(base_path().'/storage/app/data.xls'), 200, $headers);
        
        // Return the response to make the file downloadable
        //return $response;
        
        //return file_get_contents(base_path().'/storage/app/data.xls');
        return response()->download(base_path().'/storage/app/'.$filename);
        
    }
  
    public function exportW(Request $request)
    {
            //init_set("max_execution_time",3600);
            $data = DiagnosticReport::limit(20000)->get();  
            return Excel::download(new DiagnosticExport(
               
                 $data,

            ), 'report-diagnostic.xlsx');
    }

    public function datatablesX(Request $request)
    {
      
        $m = User::query();
        $recordTotal = $m->count();
       
        // if(!empty($request->name))
        // {
           
        //     $m->where('name', 'ILIKE', '%'.$request->name.'%');
        // }
        // if(!empty($request->group))
        // {
        //     $m->where('user_group_id',  $request->group);
        // }
       
        $recordFiltered = $m->count();
		
		$orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColum = $request->columns[$orderIndex]['data'];
		
		
		$m->orderBy($orderColum, $orderDir);
        $m->skip($request->start)->take($request->length);
        
       
        $data =  $m;
        return response()->json([
            'draw'              => $request->draw,
            'recordsTotal'      => $recordTotal, 
            'recordsFiltered'   => $recordFiltered,
            'data'              => $data->get(),
            'input'             => [
                'start' => $request->start,
                'draw' => $request->draw,
                'length' =>  $request->length,
                'order' => $orderIndex,
                'orderDir' => $orderDir,
                'orderColumn' => $request->columns[$orderIndex]['data']
            ]
        ]);
    }
    

    public function datatables(Request $request)
    {
      
        $m = DiagnosticReport::select('id','terminal_ext_id','terminal_sn','battery_temp','battery_percentage');
        ;
        $recordTotal = $m->count();
       
        $recordFiltered = $m->count();
		
		$orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColum = $request->columns[$orderIndex]['data'];
		
      
		
		// $m->orderBy($orderColum, $orderDir);

        $m->skip($request->start)->take($request->length);
        
        $data =  $m;
        
        // $m = $m->map(function($chunk) {
        //     return $chunk = $chunk->values();
        //  });
        
        // $data = array();
        // foreach ($m->limit(10)->get() as $key => $d) {
        //    $data[] = ['id'=>$d->id,
        //         'terminal_ext_id'=>$d->terminal_ext_id,
        //         'terminal_sn'=>$d->terminal_sn,
        //         'battery_temp'=>$d->battery_temp,
        //         'battery_percentage'=>$d->battery_percentage
                        
        //     ];

        // }
        
        return response()->json([
            'draw'              => $request->draw,
            'recordsTotal'      => $recordTotal, 
            'recordsFiltered'   => $recordFiltered,
            'data'              => $data->get(),
            //'data'              => $data,
            'input'             => [
                'start' => $request->start,
                'draw' => $request->draw,
                'length' =>  $request->length,
                'order' => $orderIndex,
                'orderDir' => $orderDir,
                'orderColumn' => $request->columns[$orderIndex]['data']
            ]
        ]);
    }
    
    public function datatablesXX(Request $request)
    {
      
        //$m = DiagnosticReport::query();
        //$recordTotal = $m->count();
       
        //$recordFiltered = $m->count();
		
        $count = DiagnosticReport::count();


		//$orderIndex = (int) $request->order[0]['column'];
        //$orderDir = $request->order[0]['dir'];
        //$orderColum = $request->columns[$orderIndex]['data'];
		
		
		//$m->orderBy($orderColum, $orderDir);
        //$m->skip($request->start)->take($request->length);
        
       
        //$data =  $m->select('id','terminal_ext_id','terminal_sn','battery_temp','battery_percentage');
        
        $data2 = array();
        $data = DiagnosticReport::chunk(200, function ($q) use ($data2) { 
            foreach ($q as $d) {
                // return ['id'=>$d->id,
                //         'terminal_ext_id'=>$d->terminal_ext_id,
                //         'terminal_sn'=>$d->terminal_sn,
                //         'battery_temp'=>$d->battery_temp,
                //         'battery_percentage'=>$d->battery_percentage
                        
                //  ];
                 $p= ['id'=>$d->id,
                        'terminal_ext_id'=>$d->terminal_ext_id,
                        'terminal_sn'=>$d->terminal_sn,
                        'battery_temp'=>$d->battery_temp,
                        'battery_percentage'=>$d->battery_percentage
                        
                 ];
                 //echo $d->terminal_sn;
                 array_push($data2,$p);
                 //return $data2;
                //echo $d->id;
            } 
          });
        
        var_dump($data);
        return response()->json([
            'draw'              => $request->draw,
            'recordsTotal'      => $count, //$recordTotal, 
            'recordsFiltered'   => $count, //$recordFiltered,
            'data'              => collect($data2),
            // 'input'             => [
            //     'start' => $request->start,
            //     'draw' => $request->draw,
            //     'length' =>  $request->length,
            //     'order' => $orderIndex,
            //     'orderDir' => $orderDir,
            //     'orderColumn' => $request->columns[$orderIndex]['data']
            // ]
        ]);
    }
}
