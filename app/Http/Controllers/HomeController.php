<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
      
        //$request->session()->invalidate();
        //$request->session()->regenerateToken();

        $data['title'] = 'Home';
        return view('home', $data);
    }

    

	public function data(Request $request)
    {
        //$data['title'] = 'Change Password';
        //return view('user/password', $data);
		$request->session()->invalidate();
        $request->session()->regenerateToken();
		echo "data";
    }

    public function datatables(Request $request)
    {
      
        $m = User::with(['userGroup']);
		
		

        $recordTotal = $m->count();
       
        if(!empty($request->name))
        {
           
            $m->where('name', 'ILIKE', '%'.$request->name.'%');
        }
        if(!empty($request->group))
        {
            $m->where('user_group_id',  $request->group);
        }
       
        $recordFiltered = $m->count();
		
		$orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColum = $request->columns[$orderIndex]['data'];
		
		
		$m->orderBy($orderColum, $orderDir);
        $m->skip($request->start)->take($request->length);
        
       
        $data =  $m;
       
		
		


        return response()->json([
            'draw'              => $request->draw,
            'recordsTotal'      => $recordTotal, 
            'recordsFiltered'   => $recordFiltered,
            'data'              => $data,
            'input'             => [
                'start' => $request->start,
                'draw' => $request->draw,
                'length' =>  $request->length,
                'order' => $orderIndex,
                'orderDir' => $orderDir,
                'orderColumn' => $request->columns[$orderIndex]['data']
            ]
        ]);
    }
}
