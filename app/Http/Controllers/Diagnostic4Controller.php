<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\DiagnosticReport;
use App\Models\Diagnostic;
// use App\Models\DiagnosticInfo;

//use App\Exports\DiagnosticExport;
use App\Exports\DiagnosticCollectionExport;

use Maatwebsite\Excel\Facades\Excel;
//use DateTime;
//use Illuminate\Support\Str;
//use Spatie\SimpleExcel\SimpleExcelWriter;
use Illuminate\Support\Facades\DB;
//use Illuminate\Support\Collection;
use Response;
//use Symfony\Component\HttpFoundation\StreamedResponse;
//use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
//use Illuminate\Support\Facades\File;
//use App\Jobs\ExportDiag;
//use Illuminate\Support\Facades\Redis;
// use Carbon;
// use Illuminate\Support\Facades\Cache;

	
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Diagnostic4Controller extends Controller
{

    public function index(Request $request)
    {
        // $produk = Cache::remember('tms_v_terminal_last_diagnostic_info', now()->addHours(1), function () {
        //     return DB::table('tms_v_terminal_last_diagnostic_info')->get();
        // });

        // //dd($produk);
        // return response()->json($produk);
        ini_set('max_execution_time', 3600); // 3600 seconds = 60 minutes
        set_time_limit(3600);
       $f =  DB::table('tms_v_terminal_last_diagnostic_info')->get();
       return response()->json($f);

    }

    public function excel3()
    {
       
        return Excel::download(new DiagnosticCollectionExport,"texxx.xlsx");
    }

    public function excel()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        
        $sheet->setCellValue('A1', 'No');
        $sheet->setCellValue('B1', 'Terminal Ext Id');
        $sheet->setCellValue('C1', 'Terminal Id');
        $sheet->setCellValue('D1', 'Terminal SN');
        $sheet->setCellValue('E1', 'Battery Temp');
        $sheet->setCellValue('F1', 'Battery Percentage');

        $results = Diagnostic::query()->select(
            'terminal_ext_id',
            'terminal_id', 
            'terminal_sn',
            'battery_temp',
            'battery_percentage',
            'latitude',
            'longitude',
            'meid',
            'switching_times',
            'swiping_card_times',
            'dip_inserting_times',
            'nfc_card_reading_times',
            'front_camera_open_times',
            'rear_camera_open_times',
            'charge_times',
            'total_memory',
            'available_memory',
            'total_flash_memory',
            'available_flash_memory',
            'total_mobile_data',
            'total_boot_time',
            'total_length_printed',
            'installed_apps'
            )->get();

        $i = 2;
        $no = 1;
        foreach ($results as $d) 
        {
            $sheet->setCellValue('A'.$i, $no++);
            $sheet->setCellValue('B'.$i, $d->terminal_ext_id);
            $sheet->setCellValue('C'.$i, $d->terminal_id);
            $sheet->setCellValue('D'.$i, $d->terminal_sn);
            $sheet->setCellValue('E'.$i, $d->battery_temp);    
            $sheet->setCellValue('F'.$i, $d->battery_percentage);    
            $i++;
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save(base_path().'/storage/app/exports/waya-data.xlsx'); 
    }


    public function excel2()
    {
        try {
           
            //====== batas == 
            // $filename = $this->fileName; // time()."-data-report-diagnotics.xls";
            // $output=fopen(base_path().'/storage/app/'.$filename,'w+');
    
          
            // fputcsv($output, [
            //     'No',
            //     'Terminal Ext Id', 
            //     'Terminal Id',
            //     'Terminal SN',
            //     'Battery Temp',
            //     'Battery Percentage',
            //     'Latitude',
            //     'Longitude',
            //     'Meid',
            //     'Switching times',
            //     'Swiping card times',
            //     'Dip Inserting Times',
            //     'Nfc Card reading times',
            //     'Front camera open times',
            //     'Rear Camera Open times',
            //     'Charge times',
            //     'Total memory',
            //     'Available memory',
            //     'Total Flash Memory',
            //     'Available Flash Memory',
            //     'Total Mobile data',
            //     'Total Boot time',
            //     'Total Length printed',
            //     'Installed Apps'
            //     ],
            //     "\t",'"');
            //     $chunk = 50000;
                // $results = DiagnosticReport::query()->select(
                //     'terminal_ext_id',
                //     'terminal_id', 
                //     'terminal_sn',
                //     'battery_temp',
                //     'battery_percentage',
                //     'latitude',
                //     'longitude',
                //     'meid',
                //     'switching_times',
                //     'swiping_card_times',
                //     'dip_inserting_times',
                //     'nfc_card_reading_times',
                //     'front_camera_open_times',
                //     'rear_camera_open_times',
                //     'charge_times',
                //     'total_memory',
                //     'available_memory',
                //     'total_flash_memory',
                //     'available_flash_memory',
                //     'total_mobile_data',
                //     'total_boot_time',
                //     'total_length_printed',
                //     'installed_apps'
        
                //     )->get();
                    
            //         foreach ($results as $row) 
            //         {
                        
                        
            //             //$no = session()->get('nodiag');
            //             $no = intval(Redis::command('get', ['nodiag'])); 
                       
                       
            //             fputcsv($output, 
            //             [
            //             $no,
            //             $row->terminal_ext_id, 
            //             $row->terminal_id, 
            //             $row->terminal_sn,
            //             $row->battery_temp,
            //             $row->battery_percentage,
            //             $row->latitude,
            //             $row->longitude,
            //             $row->meid,
            //             $row->switching_times,
            //             $row->swiping_card_times,
            //             $row->dip_inserting_times,
            //             $row->nfc_card_reading_times,
            //             $row->front_camera_open_times,
            //             $row->rear_camera_open_times,
            //             $row->charge_times,
            //             $row->total_memory,
            //             $row->available_memory,
            //             $row->total_flash_memory,
            //             $row->available_flash_memory,
            //             $row->total_mobile_data,
            //             $row->total_boot_time,
            //             $row->total_length_printed,
            //             $row->installed_apps
            //            ],
            //             "\t",'"');
                       
            //             $no = $no + 1;
            //             //session(['nodiag' => $no]);
                       
            //             Redis::set('nodiag', $no);
                       
            //         }
            //     //});
                
            //     fclose($output);
            /*
            Excel::store(function ($excel) use ($results) {
                $excel->sheet('report', function ($sheet) use ($results) {
                    $sheet->row(1, [
                            'No',
                            'Terminal Ext Id', 
                            'Terminal Id',
                            'Terminal SN',
                            'Battery Temp',
                            'Battery Percentage',
                            'Latitude',
                            'Longitude',
                            'Meid',
                            'Switching times',
                            'Swiping card times',
                            'Dip Inserting Times',
                            'Nfc Card reading times',
                            'Front camera open times',
                            'Rear Camera Open times',
                            'Charge times',
                            'Total memory',
                            'Available memory',
                            'Total Flash Memory',
                            'Available Flash Memory',
                            'Total Mobile data',
                            'Total Boot time',
                            'Total Length printed',
                            'Installed Apps'
                            ]);

                    //$sheet->appendRow($this->columns());
                    // $query->chunk(1000, function ($rows) use ($sheet) {
                    //     foreach ($rows as $row) {
                    //         $sheet->appendRow($this->rows($row));
        
                    //     }
                    // });
                    $row = 2;
                    $no=1;
                    foreach ($results as $row) {
                        //$no = intval(Redis::command('get', ['nodiag'])); 
                        //$sheet->appendRow($this->rows($row));
                        $sheet->row($row,[
                        $no,
                        $row->terminal_ext_id, 
                        $row->terminal_id, 
                        $row->terminal_sn,
                        $row->battery_temp,
                        $row->battery_percentage,
                        $row->latitude,
                        $row->longitude,
                        $row->meid,
                        $row->switching_times,
                        $row->swiping_card_times,
                        $row->dip_inserting_times,
                        $row->nfc_card_reading_times,
                        $row->front_camera_open_times,
                        $row->rear_camera_open_times,
                        $row->charge_times,
                        $row->total_memory,
                        $row->available_memory,
                        $row->total_flash_memory,
                        $row->available_flash_memory,
                        $row->total_mobile_data,
                        $row->total_boot_time,
                        $row->total_length_printed,
                        $row->installed_apps
                        ]);
                        $row++;
                        $no++;
                    }

                    // $sheet->appendRow(2, array(
                    //     'appended', 'appended'
                    // ));
                });
            },'waya.xlsx'); 
            */
            //base_path().'/storage/app/texs.xlsx'
            //->store('xlsx', base_path().'/storage/app/exports/');//->export('xlsx');
           // base_path().'/storage/app/tex.xlsx'.$filename

            

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    

}