<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\DiagnosticReport;
use App\Models\Diagnostic;
use App\Models\DiagnosticInfo;

use App\Exports\DiagnosticExport;
use App\Exports\DiagnosticCollectionExport;

use Maatwebsite\Excel\Facades\Excel;
use DateTime;
use Illuminate\Support\Str;
use Spatie\SimpleExcel\SimpleExcelWriter;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Illuminate\Support\Facades\File;
use App\Jobs\ExportDiag;
use Illuminate\Support\Facades\Redis;
//use Carbon;

class Diagnostic3Controller extends Controller
{
    
    public function index(Request $request)
    {
      
        $data['title'] = 'Diagnostic Report';
        $data['proses'] = Redis::command('get', ['nodiag']);  //session()->get('ckdiag');
        $data['tes'] = Redis::command('get', ['nodiag']);
        //echo Redis::command('get', ['ckdiag']);
        return view('pages.diagnostic3.index', $data);
    }
    public function detail(Request $request)
    {
        $data['title'] = 'Diagnostic Detail Report'; 
        $data['det'] = DiagnosticReport::where('id',$request->id)->first();
        return view('pages.diagnostic3.detail', $data);
    }
    public function export(){
        //echo 'tes';
        
        Redis::set('totaldiag',  DiagnosticReport::count());
        //Redis::set('totaldiag',  50000);
        Redis::set('ckdiag',  1);
        Redis::set('nodiag',  1);

        $porses = Redis::command('get', ['ckdiag']); //session()->get('ckdiag');
        $fileName =  time()."-data-report-diagnotics.xls";
        //$fileName =  time()."-data-report-diagnotics.csv";
        Redis::set('filediag',  $fileName);
        ExportDiag::dispatch($porses,$fileName)->onQueue('Diag1')->delay(now()->addSeconds(3));

        return response()->json([
            'responseCode' => 200,
            'responseMessage' => 'Export',
            'nodiag' =>  intval(Redis::command('get', ['nodiag'])),
            'totaldiag' => intval(Redis::command('get', ['totaldiag'])),
            'ckdiag' => intval($porses), //session()->get('ckdiag'),
            'fileName' =>  $fileName
            
        ]); 
        
    }

    public function exportxx(Request $request){
        $begin = memory_get_usage();
        $handle = fopen('export.csv', 'w');
    
        DiagnosticReport::chunk(1000, function ($persons) use ($handle) {
            foreach ($persons as $row) {
                fputcsv($handle, $row->toArray(), ';');
            }
        });
    
        fclose($handle);
        $end = (memory_get_usage()-$begin);
        echo "Memory Usage: " . ($end/1048576) . " MB \n";
        
        /*$begin = memory_get_usage();
        $data = DiagnosticReport::all();
        $handle = fopen('export.csv', 'w');
        foreach ($data as $row) {
            fputcsv($handle, $row->toArray(), ';');
        }
        fclose($handle);
        $end = (memory_get_usage()-$begin);
        echo "Memory Usage: " . ($end/1048576) . " MB \n";*/
    }

    public function export000()  
    {
        return Excel::download(new DiagnosticCollectionExport, 'persibxx.xlsx');
        
    }
   
    // public function updateProcessExport2(Request $request)
    // {
    //          $proses = session()->get('ckdiag');
    //          $total =  100000;//session()->get('totaldiag');
    //          //echo $proses;
    //          //echo $total; round((14000/100000)*100);
    //          if($total==0)
    //          {
    //             $percentage = 0;
    //             //echo "data: " . $percentage . "\n\n";
    //             return response()->json([
    //                 'responseCode' => 200,
    //                 'responseMessage' => 'Uploaded',
    //                 'data' => $percentage
    //             ]); 
    //          }
    //          else
    //          {
    //             $percentage = round(($proses / $total) * 100);
    //             return response()->json([
    //                 'responseCode' => 200,
    //                 'responseMessage' => 'Uploaded',
    //                 'data' => $percentage
    //             ]); 
    //          }
    // }

    // public function updateProcessExport(Request $request)
    // {
        
    //      $response = new StreamedResponse();
    //      $response->headers->set('Content-Type', 'text/event-stream');
    //      $response->headers->set('Cache-Control', 'no-cache');
    //      $response->headers->set('Connection', 'keep-alive');
    //      $response->headers->set('X-Accel-Buffering', 'no');
    //      $data = $request->ck;

    //      $response->setCallback(function() use ($data) {
    //          $proses = intval(Redis::command('get', ['nodiag'])); 
    //          $total =  intval(Redis::command('get', ['totaldiag']));  
             
    //          if($total==0)
    //          {
    //             $percentage = 0;
    //             echo "data: " . $percentage . "\n\n";
    //          }
    //          else
    //          {
    //             $percentage = round(($proses / $total) * 100);
    //             echo "data: " . $percentage . "\n\n";
                
    //          }
    //          ob_flush();
    //          flush();
    //      });
         
            

    //      $response->send();
    // }

    public function deleteFileXls(Request $request) {
        
      
        Redis::del('nodiag');
        Redis::del('totaldiag');
        Redis::del('ckdiag'); 
        Redis::del('filediag'); 
        


        unlink(base_path().'/storage/app/'.$request->fileName);
        return response()->json([
            'responseCode' => 200,
            'responseMessage' => 'Deleted'
        ]); 

    }

    public function export_xxxa($processId=1) {

        $response = new StreamedResponse();
        $response->headers->set('Content-Type', 'text/event-stream');
        $response->headers->set('Cache-Control', 'no-cache');
        $response->headers->set('Connection', 'keep-alive');
        $response->headers->set('X-Accel-Buffering', 'no');

        $response->setCallback(function() use ($processId) {
            //$process = Process::find($processId);
            $percentage =2;// round(($process->processed / $process->total) * 100);
            echo "data: " . $percentage . "\n\n";
            ob_flush();
            flush();
        });

        $response->send();

    }

    public function exportPPP()
    {
        
        $headers = [
            //'Content-type' => 'application/vnd.ms-excel',
            'Content-type' => 'application/json',
            'Content-Disposition' => 'attachment; filename=data.xls',
        ];

        // return response()->json([
        //     'responseCode' => 200,
        //     'responseMessage' => 'Exporting',
        //     'processId' => 1  text/plain    
        // ]); 

            // $data = response()->json([
            //     'responseCode' => 200,
            //     'responseMessage' => 'Exporting',
            //     'processId' => 1
            // ]);
        
        header('Content-type: application/json');
        header('Content-Disposition: tes2');

        $response = "TES";//new Response("TES");//Response("TES", 200, $headers);
        echo json_encode(["TES"=>"WERT"]);
    }

    public function downloadOnly(Request $request)  
    {
        $fileName = Redis::command('get', ['filediag']);
        return response()->download(base_path().'/storage/app/'.$fileName);
    }
    public function download(Request $request)  
    {
        $fileName = $request->fileName;
        return response()->download(base_path().'/storage/app/diagnostic/'.$fileName);
    }
 

    public function datatables(Request $request)
    {
        ini_set('max_execution_time', 3600); // 3600 seconds = 60 minutes
        set_time_limit(3600);
        $m = DiagnosticReport::query()->select('id','terminal_ext_id','terminal_sn','create_ts');
        ;
        $m->where('create_ts','<', \Carbon\Carbon::now()->subDays(2)->toDateTimeString());
        ////$m = DiagnosticInfo::select('id','meid');
        //$m->where('create_ts','<=', \Carbon\Carbon::now()->subDays(1)->toDateTimeString());
        //$m->whereDate('create_ts','>=','2024-02-03')->whereDate('create_ts','<=','2024-02-03');
        //$m->where('terminal_sn','V1E0437705');
        //$m->whereDate('create_ts','<', strtotime('-12 Days'));
        
        
        $recordTotal = 50;//$m->count();
        

        $recordFiltered = 50;//$m->count();
		
		$orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColum = $request->columns[$orderIndex]['data'];
		
      
		
		//$m->orderBy($orderColum, $orderDir);
        $m->orderBy('create_ts', 'DESC');

        $m->skip($request->start)->take($request->length);
        //$m->take($request->length);
        
        $data =  $m;
        
        // $m = $m->map(function($chunk) {
        //     return $chunk = $chunk->values();
        //  });
        
        // $data = array();
        // foreach ($m->limit(10)->get() as $key => $d) {
        //    $data[] = ['id'=>$d->id,
        //         'terminal_ext_id'=>$d->terminal_ext_id,
        //         'terminal_sn'=>$d->terminal_sn,
        //         'battery_temp'=>$d->battery_temp,
        //         'battery_percentage'=>$d->battery_percentage
                        
        //     ];

        // }
        
        return response()->json([
            'draw'              => $request->draw,
            'recordsTotal'      => $recordTotal, 
            'recordsFiltered'   => $recordFiltered,
            'data'              => $data->get(),
            //'data'              => $data,
            // 'input'             => [
            //     'start' => $request->start,
            //     'draw' => $request->draw,
            //     'length' =>  $request->length,
            //     'order' => $orderIndex,
            //     'orderDir' => $orderDir,
            //     'orderColumn' => $request->columns[$orderIndex]['data']
            // ]
        ]);
    }

    public function exportDiagSch()
    {
        Redis::set('totaldiag',  DiagnosticReport::count());
        Redis::set('ckdiag',  1);
        Redis::set('nodiag',  1);

        $porses = Redis::command('get', ['ckdiag']); 
        $fileName =  time()."-data-report-diagnotics.xls";
        Redis::set('filediag',  $fileName);
        ExportDiag::dispatch($porses,$fileName)->delay(now()->addSeconds(3));
    }
    public function reportExcel()
    {
        $data['title'] = 'Diagnostic Report';
        
        
        //$path = public_path();
        $path = storage_path('app'.DIRECTORY_SEPARATOR.'diagnostic'.DIRECTORY_SEPARATOR);
        $files = File::allFiles($path);

        //print_r($files);
        // echo '<pre>';
            //echo $files[0]->fileName;
        //    var_dump($files[0]);
        //echo '</pre>';
        //return view('pages.diagnostic3.readExcel', $data);

        $allMedia = [];


        foreach ($files as $path) {
            $files = pathinfo($path);
            $allMedia[] = $files['basename'];
        }
        //echo '<pre>';
            //echo $files[0]->fileName;
        //   var_dump($allMedia);
        //echo '</pre>';
        $data['files'] = $allMedia;
        return view('pages.diagnostic3.readExcel', $data);

    }
    
    public function reportExcel2()
    {
        //$data['title'] = 'Diagnostic Report';
        //return view('pages.diagnostic3.readExcel', $data);
        // $file = base_path().'/storage/app/diagnostic/1715211304-data-report-diagnotics-waktu-2024-05-08.23.35.04.xls';
        // $row = 1;
        // if (($handle = fopen($file, "r")) !== FALSE) {
        //     while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        //         $num = count($data);
        //         echo "<p> $num fields in line $row: <br /></p>\n";
        //         $row++;
        //         for ($c=0; $c < $num; $c++) {
        //             echo $data[$c] . "<br />\n";
        //         }
        //     }
        //     fclose($handle);
        // }

        //$this->handleReadExcel();
        $filename = 'tes.xls';
        $path = storage_path('app'.DIRECTORY_SEPARATOR.'diagnostic'.DIRECTORY_SEPARATOR.$filename);
           
        $data = Excel::load($path, function($reader) {})->get();

        if(!empty($data) && $data->count()){

            foreach ($data->toArray() as $key => $value) {

                if(!empty($value)){

                    foreach ($value as $v) {        
                        //$insert[] = ['title' => $v['title'], 'description' => $v['description']];
                        echo $v['No']."==".['Tes'];
                    }
                }
            }

            if(!empty($insert)){
                //Item::insert($insert);
                //return back()->with('success','Insert Record successfully.');
            }
        }


    }

    public function datatablesExcel()
    {
        //$file = base_path().'/storage/app/diagnostic/'.$filename,'w+'
        
       
    }

    public function handleReadExcel()
    {
        try {
            //$file = base_path().'/storage/app/diagnostic/1715211304-data-report-diagnotics-waktu-2024-05-08.23.35.04.xls';
            //$path = storage_path($file);
            $filename = 'tes2.xlsx';//'1715211304-data-report-diagnotics-waktu-2024-05-08.23.35.04.xls';
            $path = storage_path('app'.DIRECTORY_SEPARATOR.'diagnostic'.DIRECTORY_SEPARATOR.$filename);
            $reader = ReaderEntityFactory::createReaderFromFile($path);
            $reader->open($path);

            foreach ($reader->getSheetIterator() as $sheet) {
                if($sheet->getIndex() === 0) {
                    
                    $this->readSheet($sheet);
                    break;
                }
            }

            $reader->close();

            // delete file
            //unlink($path);

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    private function readSheet($sheet)
    {
        foreach ($sheet->getRowIterator() as $i => $row)
		{

            if($i == 1) {
                continue;
            }

            if($i == 1001) {
                break;
            }
            $r = $row->toArray();

            echo $r[1]."-".$r[1]; echo '<br/>';
        }
    }
}
