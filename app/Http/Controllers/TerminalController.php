<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\TerminalReport;
use App\Exports\TerminalReportExport;
use Maatwebsite\Excel\Facades\Excel;
use DateTime;
use App\Jobs\ExportTerminal;
use Illuminate\Support\Facades\Redis;
use Symfony\Component\HttpFoundation\StreamedResponse;


class TerminalController extends Controller
{
    
    public function index(Request $request)
    {
        
       
        //$id = time();
        //Redis::set('id-'.$id,  $id);
        
        $data['title'] = 'Terminal';
        $data['proses'] = Redis::command('get', ['noterm']); 
        //$data['id_time'] =  $id;
        return view('pages.terminal.index', $data);

        
    }

    public function detail(Request $request)
    {
        $data['title'] = 'Terminal Detail Report'; 
        $data['det'] = TerminalReport::where('sn',$request->sn)->first();
        return view('pages.terminal.detail', $data);
    }

	// public function data(Request $request)
    // {
    //     //$data['title'] = 'Change Password';
    //     //return view('user/password', $data);
	// 	$request->session()->invalidate();
    //     $request->session()->regenerateToken();
	// 	//echo "data";
    // }

    public function datatables(Request $request)
    {
      
        $m = TerminalReport::query();
        $recordTotal =50;// $m->count();
       
        // if(!empty($request->name))
        // {
           
        //     $m->where('name', 'ILIKE', '%'.$request->name.'%');
        // }
        // if(!empty($request->group))
        // {
        //     $m->where('user_group_id',  $request->group);
        // }
       
        $recordFiltered =50;// $m->count();
		
		$orderIndex = (int) $request->order[0]['column'];
        $orderDir = $request->order[0]['dir'];
        $orderColum = $request->columns[$orderIndex]['data'];
		
        $m->orderBy('sn', 'DESC');
		//$m->orderBy($orderColum, $orderDir);
        $m->skip($request->start)->take($request->length);
        
       
        $data =  $m;
        return response()->json([
            'draw'              => $request->draw,
            'recordsTotal'      => $recordTotal, 
            'recordsFiltered'   => $recordFiltered,
            'data'              => $data->get(),
            'input'             => [
                'start' => $request->start,
                'draw' => $request->draw,
                'length' =>  $request->length,
                'order' => $orderIndex,
                'orderDir' => $orderDir,
                'orderColumn' => $request->columns[$orderIndex]['data']
            ]
        ]);
    }

    
    public function export(Request $request){
        
       
      
        $fileName =  time()."-data-report-terminal.xls";
        
        Redis::set('totalterm',  TerminalReport::count());
        Redis::set('ckterm',  1);
        Redis::set('noterm',  1);
        Redis::set('fileterm',  $fileName);
        $porses = Redis::command('get', ['ckterm']);
        

        ExportTerminal::dispatch($porses,$fileName)->onQueue('Term1')->delay(now()->addSeconds(3));

        return response()->json([
            'responseCode' => 200,
            'responseMessage' => 'Export',
            'noterm' =>  intval(Redis::command('get', ['noterm'])),
            'totalterm' => intval(Redis::command('get', ['totalterm'])),
            'ckterm' => intval($porses),
            'fileName' =>  $fileName
            
        ]); 

    }
   
    public function updateProcessExport(Request $request)
    {
         //--
         $response = new StreamedResponse();
         $response->headers->set('Content-Type', 'text/event-stream');
         $response->headers->set('Cache-Control', 'no-cache');
         $response->headers->set('Connection', 'keep-alive');
         $response->headers->set('X-Accel-Buffering', 'no');
         $id = $request->ck;//session()->get('ckdiag');

         $response->setCallback(function() use ($id) {
            // $proses = intval(Redis::command('get', ['ckdiag'])); // $data;
             $proses = intval(Redis::command('get', ['noterm'])); 
             $total =  intval(Redis::command('get', ['totalterm']));  //session()->get('totaldiag');
             //echo $proses;
             //echo $total; round((14000/100000)*100);
             if($total==0)
             {
                $percentage = 0;
                echo "data: " . $percentage . "\n\n";
             }
             else
             {
                $percentage = round(($proses / $total) * 100);
                echo "data: " . $percentage . "\n\n";
                
             }
             ob_flush();
             flush();
         });
         
            

             $response->send();
    }
    public function deleteFileXls(Request $request) {
        
        //$id =  $request->id;
        Redis::del('noterm');
        Redis::del('totalterm');
        Redis::del('ckterm');
        Redis::del('fileterm');


        unlink(base_path().'/storage/app/'.$request->fileName);
        return response()->json([
            'responseCode' => 200,
            'responseMessage' => 'Deleted'
        ]); 

    }
    public function downloadOnly(Request $request)  
    {
        $fileName = Redis::command('get', ['fileterm']);
        return response()->download(base_path().'/storage/app/'.$fileName);
    }
}
