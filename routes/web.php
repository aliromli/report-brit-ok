<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\TerminalController;
use App\Http\Controllers\Terminal3Controller;
use App\Http\Controllers\DiagnosticController;
use App\Http\Controllers\Diagnostic3Controller;
use App\Http\Controllers\HomeController;
//use App\Http\Controllers\HeartBeatController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
//use Illuminate\Filesystem\Filesystem;
//use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\ProcessDiagnostic3Controller;
use App\Http\Controllers\ProcessTerminal3Controller;
use App\Http\Controllers\Diagnostic4Controller;


//Auth::routes();

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home', ['title' => 'Home']);
// })->name('home');

Route::get('/', [HomeController::class, 'index']);
Route::get('/home', [HomeController::class, 'index']);


//Route::get('register', [UserController::class, 'register'])->name('register');
//Route::post('register', [UserController::class, 'register_action'])->name('register.action');
Route::get('login', [UserController::class, 'login'])->name('login');
Route::post('login', [UserController::class, 'login_action'])->name('login.action');
//Route::get('password', [UserController::class, 'password'])->name('password');
//Route::post('password', [UserController::class, 'password_action'])->name('password.action');
//Route::get('logout', [UserController::class, 'logout'])->name('logout');
Route::post('logout', [UserController::class, 'logout'])->name('logout');
//Route::get('data', [UserController::class, 'data'])->name('data');

Route::middleware(['auth'])->group(function () {
    
    //Route::get('diagnostic', [DiagnosticController::class, 'index'])->name('diagnostic');
    //Route::get('diagnostic-datatables', [DiagnosticController::class, 'datatables'])->name('diagnostic.datatables');
    //Route::get('diagnostic-detail/{id}', [DiagnosticController::class, 'detail'])->name('diagnostic.detail');
    //Route::post('diagnostic-export', [DiagnosticController::class, 'export'])->name('diagnostic.export');
    //Route::post('diagnostic-download', [DiagnosticController::class, 'downloadOnly'])->name('diagnostic.downloadOnly');
    //Route::post('process-export-diagnostic2', [DiagnosticController::class,'updateProcessExport2'])->name('diagnostic.exportProcess2');
    //Route::post('delete-file-export-diagnostic', [DiagnosticController::class,'deleteFileXls'])->name('diagnostic.deleteFileXls');

    Route::get('diagnostic', [Diagnostic3Controller::class, 'index'])->name('diagnostic3');
    Route::get('diagnostic-datatables3', [Diagnostic3Controller::class, 'datatables'])->name('diagnostic3.datatables');
    Route::get('diagnostic-detail3/{id}', [Diagnostic3Controller::class, 'detail'])->name('diagnostic3.detail');
    Route::post('diagnostic-export3', [Diagnostic3Controller::class, 'export'])->name('diagnostic3.export');
    Route::post('diagnostic-download3', [Diagnostic3Controller::class, 'downloadOnly'])->name('diagnostic3.downloadOnly');
    //Route::post('process-export-diagnostic3', [Diagnostic3Controller::class,'updateProcessExport2'])->name('diagnostic3.exportProcess2');
    Route::post('delete-file-export-diagnostic3', [Diagnostic3Controller::class,'deleteFileXls'])->name('diagnostic3.deleteFileXls');
    Route::get('diagnostic3-datatable-excel', [Diagnostic3Controller::class, 'datatablesExcel'])->name('diagnostic3.datatablesExcel');
    Route::get('diagnostic3-report', [Diagnostic3Controller::class, 'reportExcel'])->name('diagnostic3.reportExcel');
    Route::post('diagnostic-download-excel', [Diagnostic3Controller::class, 'download'])->name('diagnostic3.download');
    
    
   
    Route::get('terminal', [TerminalController::class, 'index'])->name('terminal');
    Route::get('terminal-datatables', [TerminalController::class, 'datatables'])->name('terminal.datatables');
    Route::get('terminal-detail/{sn}', [TerminalController::class, 'detail'])->name('terminal.detail');
    Route::post('terminal-export', [TerminalController::class, 'export'])->name('terminal.export');

    Route::post('terminal-download', [TerminalController::class, 'downloadOnly'])->name('terminal.downloadOnly');
    //Route::post('process-export-terminal', [TerminalController::class,'updateProcessExport'])->name('terminal.exportProcess2');
    Route::post('delete-file-export-terminal', [TerminalController::class,'deleteFileXls'])->name('terminal.deleteFileXls');


    Route::get('terminal-report', [Terminal3Controller::class, 'reportExcel'])->name('terminal3.reportExcel');
    Route::post('terminal-download-excel', [Terminal3Controller::class, 'download'])->name('terminal3.download');
    

    Route::get('diagnostic4-download', [Diagnostic4Controller::class, 'excel'])->name('diagnostic4');
    Route::get('diagnostic4', [Diagnostic4Controller::class, 'index'])->name('diagnostic4.index');
   
    
    // Route::get('heart-beat', [HeartBeatController::class, 'index'])->name('heartbeat');
    // Route::get('heart-beat-datatables', [HeartBeatController::class, 'datatables'])->name('heartbeat.datatables');
    // Route::get('heart-beat-detail/{sn}', [HeartBeatController::class, 'detail'])->name('heartbeat.detail');
    // Route::post('heart-beat-export', [HeartBeatController::class, 'export'])->name('heartbeat.export');


    // Route::get('last-heart-beat', [LastHeartBeatController::class, 'index'])->name('lastheartbeat');
    // Route::get('last-heart-beat-datatables', [LastHeartBeatController::class, 'datatables'])->name('lastheartbeat.datatables');
    // Route::get('last-heart-beat-detail/{sn}', [LastHeartBeatController::class, 'detail'])->name('lastheartbeat.detail');
    // Route::post('last-heart-beat-export', [LastHeartBeatController::class, 'export'])->name('lastheartbeat.export');

    // Route::get('terminal-last-update', [TerminalLastUpdateController::class, 'index'])->name('terminalLastUpdate');
    // Route::get('terminal-last-update-datatables', [TerminalLastUpdateController::class, 'datatables'])->name('terminalLastUpdate.datatables');
    // Route::get('terminal-last-update-detail/{sn}', [TerminalLastUpdateController::class, 'detail'])->name('terminalLastUpdate.detail');
    // Route::post('terminal-last-update-export', [TerminalLastUpdateController::class, 'export'])->name('terminalLastUpdate.export');

    // Route::get('terminal-ack-update', [TerminalAckUpdateController::class, 'index'])->name('terminalAckUpdate');
    // Route::get('terminal-ack-update-datatables', [TerminalAckUpdateController::class, 'datatables'])->name('terminalAckUpdate.datatables');
    // Route::get('terminal-ack-update-detail/{sn}', [TerminalAckUpdateController::class, 'detail'])->name('terminalAckUpdate.detail');
    // Route::post('terminal-ack-update-export', [TerminalAckUpdateController::class, 'export'])->name('terminalAckUpdate.export');


});
//log-viewers
Route::get('log-viewers', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);
Route::get('process-export-diagnostic', [DiagnosticController::class,'updateProcessExport'])->name('diagnostic.exportProcess');
//Route::get('process-export-diagnostic3/{ck}', [Diagnostic3Controller::class,'updateProcessExport'])->name('diagnostic3.exportProcess');
//Route::get('process-export-terminal/{ck}', [TerminalController::class,'updateProcessExport'])->name('terminal.exportProcess');
Route::get('export-diag-sch', [Diagnostic3Controller::class,'exportDiagSch'])->name('diagnostic3.exportDiagSch');
Route::get('process-export-diagnostic3/{ck}', [ProcessDiagnostic3Controller::class,'updateProcessExport'])->name('diagnostic3.exportProcess');
Route::get('process-export-terminal/{ck}', [ProcessTerminal3Controller::class,'updateProcessExport'])->name('terminal.exportProcess');


route::get('tes-waktu', function(){

    $ldate = date('Y-m-d H:i:s');
    $ldate = str_replace(" ",".",$ldate);
    $ldate = str_replace(":",".",$ldate);

    echo $ldate;
    
});