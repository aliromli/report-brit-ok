@if ($menu->children->count() == 0)
   
        <li class="{{ url($menu->action_url) == url()->current()?'active':'' }} top-menu-invisible">
             @if(!empty($menu->icon))
               <a href="{{($menu->action_url == '#')?'#':url($menu->action_url)}}" title=""><img src="{{asset('assets/images/icon')}}/{{$menu->icon}}"/> <span class="menu-item-parent">{{strtolower($menu->name)}}</span></a>
             @else
              <a href="{{($menu->action_url == '#')?'#':url($menu->action_url)}}" title=""><span class="menu-item-parent">{{strtolower($menu->name)}}</span></a>
            @endif
        </li>
   
@else
    <li class="top-menu-invisible">
        @if(!empty($menu->icon))
            <a href="{{($menu->action_url == '#')?'#':url($menu->action_url)}}" title=""><img src="{{asset('assets/images/icon')}}/{{$menu->icon}}"/> <span class="menu-item-parent">{{strtolower($menu->name)}}</span></a>
        @else
            <a href="{{($menu->action_url == '#')?'#':url($menu->action_url)}}" title=""><span class="menu-item-parent">{{strtolower($menu->name)}}</span></a>
        @endif
            <ul>
            @foreach($menu->children as $menu)
                @if(in_array(collect($menu->actions)->firstWhere('action_type', 'READ')['id'], collect(Auth::user()->userGroup->menuActions)->pluck('id')->toArray()))
                    @include('layouts.menu', $menu)
                @endif
            @endforeach
        </ul>
    </li>
@endif
