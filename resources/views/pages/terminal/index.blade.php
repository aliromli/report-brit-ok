@extends('layouts.app')
@section('title', 'Terminall')
@section('ribbon')
@endsection

@section('content')
<div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Terminal <small>Report</small></h3>
              </div>
            </div>
            <input type="hidden" value="{{$proses}}" id="proses">
            <input type="hidden" id="fileName">

            

            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12">           
                <button class="btn btn-success source" id="export-excel-terminal"><i class="fa fa-file-excel-o"></i>&nbsp;Excel</button>
              </div>
              <span id="div-progress" style="display:none;" class="w-100">
                <div class="col-md-6 col-sm-6">    
                  <div class="progress">
                        <div id="progressbar" class="progress-bar progress-bar-striped" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>  
                <div class="col-md-6 col-sm-6">  
                    <div class="progress-text">0% Completed ... Please wait ...</div>
                </div>
              </span>
              <div class="col-md-12 col-sm-12" style="display:none;">
                <div class="x_panel">
                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                    <p class="text-muted font-13 m-b-30">
                    </p>
                    <table id="dataTableTerminal" class="table table-striped table-bordered" style="width:100%">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>SN</th>
                          <th>IMEI</th>
                          <th>Terminal Id</th>
                          <th>Merchant Name 1</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                  </div>
              </div>
            </div>
                </div>
              </div>
        </div>


</div>
@endsection


@section('script')
    <!-- Datatables -->
    <script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('vendors/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('vendors/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('vendors/pdfmake/build/vfs_fonts.js') }}"></script>
    <!-- <script src="{{ asset('assets/js/terminal.js') }}"></script> -->
    <script>
      $(document).ready(function () {
        /*
        var dataTableDiagnostic = null;
        dataTableDiagnostic =  $('#dataTableTerminal').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        "info": false,
        "lengthChange": false,
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/terminal-datatables", 
            type: 'GET',
            data:  function(d){
                //d.state = $('#search-state').val(); 
                //d.country = $('#search-state-country').val();
                
            }
        },
        language: {
          
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "sn", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "sn", name: "sn", visible:true},
            {data: "imei", name: "imei", visible:true},
            {data: "terminal_id", name: "terminal_id"},
            {data: "merchant_name1", name: "merchant_name1"},
            {data: "id", sortable: false, searchable: false, }
        ],

        columnDefs:[
            {
                "targets": 0, // your case first column
                "className": "text-center",
            },
            {
                targets: 5,
                "className": "text-center padd-row-table",
                render: function(d,data,row) {
                    var sn = row.sn;
                    return `<span>
                    <a href="terminal-detail/`+sn+`"  class="" data-id="`+d+`">View</a>&nbsp;
                    </span>`;
                    
                }
            }
        ]
        });
        */
    

        $('body').on('click', '#export-excel-terminal', function() 
          {
            $('#div-progress').show();
            $('.progress-text').html('0% Completed ... Please wait ...');
              // Create form data
            //var form = new FormData();
            //form.append('id', $('#proses').val());

             // Show loader
             $('.page-loader').removeClass('hidden');
                $.ajax({
                    url:baseUrl+"/terminal-export", 
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    processData: false, // prevent jQuery from automatically transforming the data into a query string
                    contentType: false, // is imperative, since otherwise jQuery will set it incorrectly.
                    //data: {"id":$('#proses').val()},
                    success: function(resp) {
                        if(resp.responseCode === 200) {
                            console.log(resp.ckterm,'-',resp.fileName);
                            $("#proses").val(resp.ckterm); 
                            $("#fileName").val(resp.fileName);
                            checkProcess(resp.ckterm);
                            //checkProcess($('#proses').val());
                        } else {
                            $("#proses").val("");
                            // $.smallBox({
                            //     height: 50,
                            //     title : "Error",
                            //     content : resp.responseMessage,
                            //     color : "#dc3912",
                            //     sound_file: "smallbox",
                            //     timeout: 3000
                            // });
                        }
                        //$('.page-loader').addClass('hidden');
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                               $("#proses").val("");
                              //  $.smallBox({
                              //       title : "Error",
                              //       content : xhr.statusText,
                              //       color : "#dc3912",
                              //       sound_file: "smallbox",
                              //       timeout: 3000
                              //   });
                              //   $('.page-loader').addClass('hidden');
                   }
                });


          });

             // Check process
            function checkProcess(data)
            {
                 $('#div-progress').show();
                 
 
                // Check browser support Server-Send Event
                if (typeof (EventSource) !== "undefined") {
                    // Yes! Server-sent events support!
                    var process = 0;
                    var url = "{{ url('process-export-terminal') }}/"+data;
                    var source = new EventSource(url);
                    source.onmessage = function (e) {

                       // mcuDataTable.ajax.reload();
                        //console.log(e.data)
                        if (process !== parseInt(e.data)) {
                          $('#progressbar').attr('aria-valuenow', e.data).attr('style', 'width:'+ e.data+'%');
                          $('.progress-text').html(e.data+'% Completed ... Please wait ...');
           
                        }

                        if(parseInt(process) === 0) {
                            $('.progress-text').html('Calculate rows... Please wait ...');
                        }

                        if(parseInt(process) === 100 || parseInt(process) > 100) {

                            //$('#progressbar').attr('aria-valuenow', e.data).attr('style', 'width: 0%');

                            $('#progressbar').attr('aria-valuenow', 0).attr('style', 'width: 0%');
                            //alert("Uploading Success !");
                            source.close();
                            $("#proses").val("");
                            download($("#fileName").val());
                        }
                
                        process = parseInt(e.data);
                    };
                } else {
                    // Sorry! No server-sent events support..
                    //toastr.error('No server-sent events support', 'Error');
                    console.log('error', 'No server-sent events support');
                  
                    $('#div-progress').show();
                    $("#proses").val("");
                }
            }


        // Check if current process is running
        @if($proses)
            checkProcess({{ $proses }});
        @endif   

      });

    function download(fileName)
    {
               $('.progress-text').html('Please wait Downloding...');  
                var data = new FormData();
                data.append('fileName',fileName);
                $.ajax({
                    url: baseUrl+"/terminal-download", 
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    cache: false,
                    xhrFields: {
                        responseType: 'blob'
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                   
                    data: data,
                    success: function (data, textStatus, xhr) {
                        // check for a filename
                        //console.log("data",data)
                        //console.log("status",textStatus)
                        //console.log("xhr", xhr.getResponseHeader('Content-Disposition'))
                        //-----------
                        var filename = "";
                        var disposition = xhr.getResponseHeader('Content-Disposition');
                        if (disposition && disposition.indexOf('attachment') !== -1) {
                            var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                            var matches = filenameRegex.exec(disposition);
                            if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                            var a = document.createElement('a');
                            var url = window.URL.createObjectURL(data);
                            a.href = url;
                            a.download = filename;
                            document.body.append(a);
                            a.click();
                            a.remove();
                            window.URL.revokeObjectURL(url);
                            //console.log("data",filename)
                            $.ajax({
                                url: baseUrl+"/delete-file-export-terminal", 
                                type: 'POST',
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                               data: {
                                    'fileName':filename,
                                    //'id':$("#proses").val()
                                    
                                },
                                success: function(resp) {
                                    if(resp.responseCode === 200) {
                                      console.log(resp.responseMessage)
                                      $('#div-progress').hide();
                                      $('.progress-text').html('');
                                    } else {
                                        console.log(resp.responseMessage)
                                    }
                                    // Hide loder
                                    $('.page-loader').addClass('hidden');
                                },
                                error: function(xhr, ajaxOptions, thrownError) {
                                    console.log(xhr.statusText)
                                }
                            });
                        }
                        else {
                            alert("Error");
                        }
                    
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {

                    },

                }); 
    }
    </script>

@endsection
