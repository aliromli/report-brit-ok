@extends('layouts.app')
@section('title', 'User')
@section('ribbon')
@endsection


@section('content')
<div class="title-page">
    <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb mt-bread">
                    <li><a href="/user/profile">USER PROFILE</a></li>
                </ul>
            </div>
    </div> 
</div>
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card mb-tng2">
                <div class="card-body">
						<form align="center" enctype="multipart/form-data" action="{{ url('/user/update-profile') }}" id="login-form" class="smart-form client-form" method="post">
							@csrf
							<div class="modal-body">
								<div class="modal-header no-border" style="margin-top: -24px;">
									<i class="fa fa-user"></i> @lang('update-profile.update_profile')
									
								</div>
								
								<!--

								<div class="alert alert-warning alert-dismissible fade show" role="alert">
                                            <strong>Holy guacamole!</strong> You should check in on some of those fields below.
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
                                            </button>
                                        </div>
								-->

								<div>
									@if(session('error'))
									<div class="alert alert-warning alert-dismissible fade show" role="alert">
										<strong style="padding:5px;">Warning!</strong>
										{{ session('error') }}
										<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
									</div>
									@endif

									@if(session('success'))
									<div class="alert alert-success alert-dismissible fade show" role="alert">
										<strong style="padding:5px;">Success !</strong>
										{{ session('success') }}
										<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
									</div>
									@endif
								</div>
								
								<div class="row mt-5">
									<div class="col-md-6">
										<fieldset> 
											<div class="form-group">
												<label for="name">@lang('update-profile.upload_photo')</label>
												<input accept=".jpg,.gif,.jpeg,.png" id="file" type="file"  class="form-control form-control-sm input-xs w-80" name="file" value="{{ session()->get('user.name') }}" autofocus>
											</div>
											<div class="form-group">
												<label for="name">@lang('update-profile.name')</label>
												<input id="name" type="text"  class="form-control form-control-sm input-xs w-80"  name="name" value="{{ session()->get('user.name') }}" required autofocus>
											</div>
											<div class="form-group">
												<label for="email">@lang('update-profile.email')</label>
												<input id="email" type="email"  class="form-control form-control-sm input-xs w-80"  name="email" value="{{ session()->get('user.email') }}" required autofocus>
											</div>
											
										</fieldset>
									</div>
									<div class="col-md-6">
										<fieldset> 
											<div class="form-group">
												<div 
												style="width:100px;
                          								height:100px;
                          								position:absolute;">
													
													@if(session()->get('user.avatar') !== null)
													<img height="50" class="img-circle" src="{{ asset('storage/user') . '/' . session()->get('user.avatar') }}" data-toggle="dropdown">
													@else
													<img width="100" class="img-circle" src="{{ asset('assets/images/author/user-avatar-icon.jpg') }}" data-toggle="dropdown">
													@endif
												</div>
											</div>
											
											
										</fieldset>
									</div>
								</div>
								<footer>
									<button type="submit" class="btn btn-primary btn-block btn-rounded">
										<i class="fa fa-check-circle"></i> @lang('general.submit')
									</button>
								</footer>
							</div>
						</form>
					
				
		
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
