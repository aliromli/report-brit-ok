@extends('layouts.app')
@section('title', 'Detail Diagnostic')
@section('ribbon')
@endsection

@section('content')
<div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Diagnostic <small>Detail</small></h3>
              </div>
            </div>

            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12">           
                      <button class="btn btn-success source" onclick="window.history.go(-1); return false;"><i class="fa fa-arrow-left"></i>&nbsp;Back</button>
              </div>
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <p class="text-muted font-13 m-b-30">
                                </p>
                                <div class="col-md-6">
                          <!-- <p class="lead">Amount Due 2/22/2014</p> -->
                          <div class="table-responsive">
                          <table class="table table-striped">
                              <tbody>
                                <tr>
                                  <th style="width:50%">Terminal Ext Id</th>
                                  <td>: {{$det?$det->terminal_ext_id:''}}</td> 
                                </tr>
                                <tr>
                                  <th>Terminal SN</th>
                                  <td>: {{$det?$det->terminal_sn:''}}</td>
                                </tr>
                                <tr>
                                  <th>Battery Temp</th>
                                  <td>: {{$det?$det->battery_temp:''}}</td>
                                </tr>
                                <tr>
                                  <th>Battery Percentage</th>
                                  <td>: {{$det?$det->battery_percentage:''}}</td>
                                </tr>
                                <tr>
                                  <th>Latitude</th>
                                  <td>: {{$det?$det->latitude:''}}</td>
                                </tr>
                                <tr>
                                  <th>Longitude</th>
                                  <td>: {{$det?$det->longitude:''}}</td>
                                </tr>
                                <tr>
                                  <th>Meid</th>
                                  <td>: {{$det?$det->meid:''}}</td>
                                </tr> 
                                <tr>
                                  <th>Switching Times</th>
                                  <td>: {{$det?$det->switching_times:''}}</td>
                                </tr> 
                                <tr>
                                  <th>Swiping Card Times</th>
                                  <td>: {{$det?$det->swiping_card_times:''}}</td>
                                </tr> 
                                <tr>
                                  <th>Dip Inserting Times</th>
                                  <td>: {{$det?$det->dip_inserting_times:''}}</td>
                                </tr> 
                                <tr>
                                  <th>Nfc Card Reading Times</th>
                                  <td>: {{$det?$det->nfc_card_reading_times:''}}</td>
                                </tr> 
                                <tr>
                                  <th>Front Camera Open Times</th>
                                  <td>: {{$det?$det->front_camera_open_times:''}}</td>
                                </tr>
                                <tr>
                                  <th>Rear Camera Open Times</th>
                                  <td>: {{$det?$det->rear_camera_open_times:''}}</td>
                                </tr>
                                <tr>
                                  <th>Charge_times</th>
                                  <td>: {{$det?$det->charge_times:''}}</td>
                                </tr>
                                <tr>
                                  <th>Total Memory</th>
                                  <td>: {{$det?$det->total_memory:''}}</td>
                                </tr>
                                <tr>
                                  <th>Available Memory</th>
                                  <td>: {{$det?$det->available_memory:''}}</td>
                                </tr>
                                <tr>
                                  <th>Total Flash Memory</th>
                                  <td>: {{$det?$det->total_flash_memory:''}}</td>
                                </tr>
                                <tr>
                                  <th>Available Flash Memory</th>
                                  <td>: {{$det?$det->available_flash_memory:''}}</td>
                                </tr>
                                <tr>
                                  <th>Total Mobile Data</th>
                                  <td>: {{$det?$det->total_mobile_data:''}}</td>
                                </tr>
                                <tr>
                                  <th>Current Boot Time</th>
                                  <td>: {{$det?$det->current_boot_time:''}}</td>
                                </tr>
                                <tr>
                                  <th>Total Boot Time</th>
                                  <td>: {{$det?$det->total_boot_time:''}}</td>
                                </tr>
                                <tr>
                                  <th>Total Length Printed</th>
                                  <td>: {{$det?$det->total_length_printed:''}}</td>
                                </tr>
                                <tr>
                                  <th>Installed Apps</th>
                                  <td>: {{$det?$det->installed_apps:''}}</td>
                                </tr>
                               
                              </tbody>
                            </table>
                          </div>
                        </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
              </div>
        </div>


</div>
@endsection


@section('script')
<!-- Start datatable js -->
<!-- <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script> -->
<!-- <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script> -->
<!-- <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script> -->
<!-- <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script> -->
<!-- <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script> -->

    <!-- <script src="{{ asset('assets/js/diagnostic.js') }}"></script> -->
@endsection
