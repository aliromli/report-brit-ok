@extends('layouts.app')
@section('title', 'Diagnostic')
@section('ribbon')
@endsection

@section('content')
<div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Diagnostic <small>Report</small></h3>
              </div>
            </div>
            <!-- <input type="hidden" id="fileName"> -->
           
            
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12">           
                      <button class="btn btn-primary source" id="export-excel"><i class="fa fa-file-excel-o"></i>&nbsp;File Excel</button>
              </div>
              <!-- <span id="div-progress" style="display:none;" class="w-100">
                <div class="col-md-6 col-sm-6">    
                  <div class="progress">
                        <div id="progressbar" class="progress-bar progress-bar-striped" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>  
                <div class="col-md-6 col-sm-6">  
                    <div class="progress-text">0% Completed ... Please wait ...</div>
                </div>
              </span> -->
              <div class="col-md-12 col-sm-12" style="display:none;" id="opo">
                <div class="x_panel">
                  
                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                    <p class="text-muted font-13 m-b-30">
                    </p>
                    <table class="table table-striped table-bordered" style="width:100%">
                      <tbody>
                        <?php
                           if($files)
                           {
                              
                              foreach($files as $file)
                              {
                                $waktu = explode("waktu-",$file);
                                $waktu = str_replace(".xls","",$waktu);
                                echo "<tr><td width='45%'>File name : <span class='nm-file'>".$file."</span>
                                <div class='progress' style='display:none;'>  
                                    <div id='progressbar' class='progress-bar progress-bar-striped' role='progressbar' style='width: 0%' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100'></div>
                                </div>
                                </td><td width='20%'> Time : ".$waktu[1]."</td><td width='35%'><button class='btn btn-success source dwn-btn'><i class='fa fa-file-excel-o'></i>&nbsp;Download</button></tr></tr>";
                              }
                           }
                        ?>
                      </tbody>
                    </table>
                  </div>
                  </div>
              </div>
            </div>
                </div>
              </div>
        </div>


</div>
@endsection


@section('script')
    <!-- Datatables -->
    <script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('vendors/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('vendors/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('vendors/pdfmake/build/vfs_fonts.js') }}"></script>
    <!-- <script src="{{ asset('assets/js/diagnostic.js') }}"></script> -->
    <script>
    // $(document).ready(function () {
    //    var dataTableDiagnostic = null;
    //    dataTableDiagnostic =  $('#dataTableDiagnostic').DataTable({
    //     processing: true,
    //     serverSide: true,
    //     //dom : '',
    //     dom: 'lrtip',
    //     "searching": false,
    //     "lengthChange": false,
    //     pageLength: 10,
    //     "info": false,
    //     //lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
    //     pagingType: 'full_numbers',
    //     ajax: {
    //         url:   baseUrl+"/diagnostic3-datatable-excel", 
    //         type: 'GET',
    //         data:  function(d){
    //             d.state = $('#search-state').val(); 
    //             d.country = $('#search-state-country').val();
                
    //         }
    //     },
    //     language: {
           
    //     },
    //     //rowId: 'TRANSPORT_ID',
    //     columns: [
    //         {data: "id", 
    //            sortable: false, 
    //            searchable: false,
    //            "render": function (data, type, row, meta) {      
    //                       return meta.row + meta.settings._iDisplayStart + 1;     
    //            }  
    //         },
    //         {data: "terminal_ext_id", name: "terminal_ext_id", visible:true},
    //         {data: "terminal_sn", name: "terminal_sn"},
    //         // {data: "create_ts", name: "battery_temp"},
    //         //{data: "battery_percentage", name: "battery_percentage"},
    //         //{data: "meid", name: "meid"},
    //         {data: "id", sortable: false, searchable: false, }
    //     ],

    //     columnDefs:[
    //         {
    //             "targets": 0, // your case first column
    //             "className": "text-center",
    //         },
    //         {
    //             targets: 3,
    //             "className": "text-center padd-row-table",
    //             render: function(d,data,row) {
                  
    //                 return `<span>
    //                 <a href="diagnostic-detail/`+d+`"  class="" data-id="`+d+`">View</a>&nbsp;
    //                 </span>`;
                    
    //             }
    //         }
    //     ]
    //     });
        
        $("#export-excel").click(function(){
          $("#opo").toggle();
        })

        $(".dwn-btn").click(function(){

                var url = baseUrl+"/diagnostic-download-excel";
                var  obp = $(this).parent().parent();
                var fileName = obp.find('.nm-file').text();
                var s = obp.find('.progress'); //
                s.show();
                var p = s.find('#progressbar');
                var data = new FormData();
                data.append('fileName',fileName);
                $.ajax({
                    xhr: function() {
                        var xhr = new window.XMLHttpRequest();
                        xhr.upload.addEventListener("progress", function(evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                //Do something with upload progress here
                            }
                      }, false);

                      xhr.addEventListener("progress", function(evt) {
                          if (evt.lengthComputable) {
                              var percentComplete = evt.loaded / evt.total;
                              //Do something with download progress
                              //console.log(percentComplete);
                              let d =  Math.round(percentComplete*100);
                            
                              p.attr('aria-valuenow', d).attr('style', 'width:'+ d+'%');
                         
                          }
                      }, false);

                      return xhr;
                    },
                    type: 'POST',
                    url: url,
                    contentType: false,
                    processData: false,
                    cache: false,
                    xhrFields: {
                        responseType: 'blob'
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    //data: {fileName:fileName},
                    data: data,
                    success: function(data, textStatus, xhr){
                        //Do something on success
                        //console.log("data",data)
                        var filename = "";
                        var disposition = xhr.getResponseHeader('Content-Disposition');
                        if (disposition && disposition.indexOf('attachment') !== -1) {
                            var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                            var matches = filenameRegex.exec(disposition);
                            if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                            var a = document.createElement('a');
                            var url = window.URL.createObjectURL(data);
                            a.href = url;
                            a.download = filename;
                            document.body.append(a);
                            a.click();
                            a.remove();
                            window.URL.revokeObjectURL(url);
                            //console.log("data",filename)
                            s.hide();
                            $("#opo").toggle();
                          
                        }
                        else {
                            alert("Error");
                        }
                    }
                });
                


        
         
        });


    </script>
      
@endsection
