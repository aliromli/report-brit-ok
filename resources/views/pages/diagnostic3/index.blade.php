@extends('layouts.app')
@section('title', 'Diagnostic')
@section('ribbon')
@endsection

@section('content')
<div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Diagnostic <small>Report</small></h3>
              </div>
            </div>
            <input type="hidden" value="{{$proses}}" id="proses">
            <input type="hidden" id="fileName">
           
            
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12">           
                      <button class="btn btn-success source" id="export-excel"><i class="fa fa-file-excel-o"></i>&nbsp;Excel</button>
              </div>
              <span id="div-progress" style="display:none;" class="w-100">
                <div class="col-md-6 col-sm-6">    
                  <div class="progress">
                        <div id="progressbar" class="progress-bar progress-bar-striped" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>  
                <div class="col-md-6 col-sm-6">  
                    <div class="progress-text">0% Completed ... Please wait ...</div>
                </div>
              </span>
              <div class="col-md-12 col-sm-12" style="display:none;">
                <div class="x_panel">
                  
                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                    <p class="text-muted font-13 m-b-30">
                    </p>
                    <table id="dataTableDiagnostic" class="table table-striped table-bordered" style="width:100%">
                      <thead>
                        <tr>
                          <th>NO</th>
                          <!-- <th>MEID</th> -->
                          <th>Terminal ExtId</th>
                          <th>SN</th>
                          <th>Create Date</th>
                          <!-- <th>Battery Percentage</th>  -->
                          <th>ACTIOIN</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                  </div>
              </div>
            </div>
                </div>
              </div>
        </div>


</div>
@endsection


@section('script')
    <!-- Datatables -->
    <script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('vendors/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('vendors/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('vendors/pdfmake/build/vfs_fonts.js') }}"></script>
    <!-- <script src="{{ asset('assets/js/diagnostic.js') }}"></script> -->
    <script>
    $(document).ready(function () {
       /*var dataTableDiagnostic = null;
       dataTableDiagnostic =  $('#dataTableDiagnostic').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        "lengthChange": false,
        pageLength: 10,
        "info": false,
        //lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/diagnostic-datatables3", 
            type: 'GET',
            data:  function(d){
                d.state = $('#search-state').val(); 
                d.country = $('#search-state-country').val();
                
            }
        },
        language: {
           
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "id", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "terminal_ext_id", name: "terminal_ext_id", visible:true},
            {data: "terminal_sn", name: "terminal_sn"},
            {data: "create_ts", name: "battery_temp"},
            //{data: "battery_percentage", name: "battery_percentage"},
            //{data: "meid", name: "meid"},
            {data: "id", sortable: false, searchable: false, }
        ],

        columnDefs:[
            {
                "targets": 0, // your case first column
                "className": "text-center",
            },
            {
                targets: 4,
                "className": "text-center padd-row-table",
                render: function(d,data,row) {
                  
                    return `<span>
                    <a href="diagnostic-detail/`+d+`"  class="" data-id="`+d+`">View</a>&nbsp;
                    </span>`;
                    
                }
            }
        ]
        });
        */
        

          $('body').on('click', '#export-excel', function() 
          {
            $('#div-progress').show();
            $('.progress-text').html('0% Completed ... Please wait ...');
              // Create form data
            //var form = new FormData();
            //form.append('file', $('#file')[0].files[0]);

             // Show loader
             $('.page-loader').removeClass('hidden');
                $.ajax({
                    url:baseUrl+"/diagnostic-export3", 
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    processData: false, // prevent jQuery from automatically transforming the data into a query string
                    contentType: false, // is imperative, since otherwise jQuery will set it incorrectly.
                    //data: form,
                    success: function(resp) {
                        if(resp.responseCode === 200) {
                            console.log(resp.ckdiag,'-',resp.fileName);
                            $("#proses").val(resp.ckdiag); 
                            $("#fileName").val(resp.fileName);
                            checkProcess(resp.ckdiag);
                        } else {
                            $("#proses").val("");
                            // $.smallBox({
                            //     height: 50,
                            //     title : "Error",
                            //     content : resp.responseMessage,
                            //     color : "#dc3912",
                            //     sound_file: "smallbox",
                            //     timeout: 3000
                            // });
                        }
                        //$('.page-loader').addClass('hidden');
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                               $("#proses").val("");
                              //  $.smallBox({
                              //       title : "Error",
                              //       content : xhr.statusText,
                              //       color : "#dc3912",
                              //       sound_file: "smallbox",
                              //       timeout: 3000
                              //   });
                              //   $('.page-loader').addClass('hidden');
                   }
                });


          });

             // Check process
            function checkProcess(data)
            {
                 $('#div-progress').show();
                 
 
                // Check browser support Server-Send Event
                if (typeof (EventSource) !== "undefined") {
                    // Yes! Server-sent events support!
                    var process = 0;
                    //var url = "{{ url('process-export-diagnostic3') }}/"+data;
                    var url = baseUrl + "/process-export-diagnostic3/"+data;
                    var source = new EventSource(url);
                    source.onmessage = function (e) {

                       // mcuDataTable.ajax.reload();
                        //console.log(e.data)
                        if (process !== parseInt(e.data)) {
                          $('#progressbar').attr('aria-valuenow', e.data).attr('style', 'width:'+ e.data+'%');
                          $('.progress-text').html(e.data+'% Completed ... Please wait ...');
           
                        }

                        if(parseInt(process) === 0) {
                            $('.progress-text').html('Calculate rows... Please wait ...');
                        }

                        if(parseInt(process) === 100 || parseInt(process) > 100) {

                            $('#progressbar').attr('aria-valuenow', e.data).attr('style', 'width: 0%');
                            //alert("Uploading Success !");
                            source.close();
                            $("#proses").val("");
                            download($("#fileName").val());
                        }
                
                        process = parseInt(e.data);
                    };
                } else {
                    // Sorry! No server-sent events support..
                    //toastr.error('No server-sent events support', 'Error');
                    console.log('error', 'No server-sent events support');
                  
                    $('#div-progress').show();
                    $("#proses").val("");
                }
            }


        // Check if current process is running
        @if($proses)
            checkProcess({{ $proses }});
        @endif    

      
    });

    function download(fileName)
    {           $('.progress-text').html('Please wait Downloding...');
                var data = new FormData();
                data.append('fileName',fileName);
                $.ajax({
                    url: baseUrl+"/diagnostic-download3", 
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    cache: false,
                    xhrFields: {
                        responseType: 'blob'
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                   
                    data: data,
                    success: function (data, textStatus, xhr) {
                        // check for a filename
                        //console.log("data",data)
                        //console.log("status",textStatus)
                        //console.log("xhr", xhr.getResponseHeader('Content-Disposition'))
                        //-----------
                        var filename = "";
                        var disposition = xhr.getResponseHeader('Content-Disposition');
                        if (disposition && disposition.indexOf('attachment') !== -1) {
                            var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                            var matches = filenameRegex.exec(disposition);
                            if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                            var a = document.createElement('a');
                            var url = window.URL.createObjectURL(data);
                            a.href = url;
                            a.download = filename;
                            document.body.append(a);
                            a.click();
                            a.remove();
                            window.URL.revokeObjectURL(url);
                            //console.log("data",filename)
                            $.ajax({
                                url: baseUrl+"/delete-file-export-diagnostic3", 
                                type: 'POST',
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                               data: {
                                    'fileName':filename,
                                    
                                },
                                success: function(resp) {
                                    if(resp.responseCode === 200) {
                                      console.log(resp.responseMessage)
                                      $('#div-progress').hide();
                           
                                    } else {
                                        console.log(resp.responseMessage)
                                    }
                                    // Hide loder
                                    $('.page-loader').addClass('hidden');
                                },
                                error: function(xhr, ajaxOptions, thrownError) {
                                    console.log(xhr.statusText)
                                }
                            });
                        }
                        else {
                            alert("Error");
                        }
                    
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {

                    },

                }); 
    }

    </script>
      
@endsection
