@extends('layouts.app')
@section('title', 'Detail Terminal')
@section('ribbon')
@endsection

@section('content')
<div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Last Heart Beat <small>Detail</small></h3>
              </div>
            </div>

            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12">           
                      <button class="btn btn-success source" onclick="window.history.go(-1); return false;"><i class="fa fa-arrow-left"></i>&nbsp;Back</button>
              </div>
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <p class="text-muted font-13 m-b-30">
                                </p>
                                <div class="col-md-6">
                          <!-- <p class="lead">Amount Due 2/22/2014</p> -->
                          <div class="table-responsive">
                            <table class="table table-striped">
                              <tbody>
                                <tr>
                                  <th style="width:50%">SN</th>
                                  <td>: {{$det?$det->sn:''}}</td> 
                                </tr>
                                <tr>
                                  <th>IMEI</th>
                                  <td>: {{$det?$det->imei:''}}</td>
                                </tr>
                                <tr>
                                  <th>TerminalID</th>
                                  <td>: {{$det?$det->terminal_id:''}}</td>
                                </tr>
                                <tr>
                                  <th>Merchant_ID</th>
                                  <td>: {{$det?$det->merchant_id:''}}</td>
                                </tr>
                                <tr>
                                  <th>Merchant Name 1</th>
                                  <td>: {{$det?$det->merchant_name1:''}}</td>
                                </tr>
                                <tr>
                                  <th>Merchant Name 2</th>
                                  <td>: {{$det?$det->merchant_name2:''}}</td>
                                </tr>
                                <tr>
                                  <th>Merchant Name 3</th>
                                  <td>: {{$det?$det->merchant_name2:''}}</td>
                                </tr> 
                                <tr>
                                  <th>Feature Sale</th>
                                  <td>: <span  class="bool">{{$det?$det->feature_sale:''}}</span></td>
                                </tr> 
                                <tr>
                                  <th>Feature Sale TIP</th>
                                  <td>: <span  class="bool">{{$det?$det->feature_sale_tip:''}}</span></td>
                                </tr> 
                                <tr>
                                  <th>Feature Sale TIP</th>
                                  <td>: <span  class="bool">{{$det?$det->feature_installment:''}}</span></td>
                                </tr> 
                                <tr>
                                  <th>Feature Sale TIP</th>
                                  <td>: <span  class="bool">{{$det?$det->feature_sale_fare_non_fare:''}}</span></td>
                                </tr> 
                                <tr>
                                  <th>Feature Sale TIP</th>
                                  <td>: <span  class="bool">{{$det?$det->feature_manual_key_in:''}}</span></td>
                                </tr>
                                <tr>
                                  <th>Feature QRIS</th>
                                  <td>: <span  class="bool">{{$det?$det->feature_qris:''}}</span></td>
                                </tr>
                                <tr>
                                  <th>Feature ContactLess</th>
                                  <td>: <span  class="bool">{{$det?$det->feature_contactless:''}}</span></td>
                                </tr>
                                <tr>
                                  <th>Random Pin keypad</th>
                                  <td>: <span  class="bool">{{$det?$det->random_pin_keypad:''}}</span></td>
                                </tr>
                                <tr>
                                  <th>Auto Logon</th>
                                  <td>: <span  class="bool">{{$det?$det->auto_logon:''}}</span></td>
                                </tr>
                                <tr>
                                  <th>Next Logon</th>
                                  <td>: {{$det?$det->next_logon:''}}</td>
                                </tr>
                                <tr>
                                  <th>Installment Options 1</th>
                                  <td>: {{$det?$det->installment1_options:''}}</td>
                                </tr>
                                <tr>
                                  <th>Installment Options 2</th>
                                  <td>: {{$det?$det->installment2_options:''}}</td>
                                </tr>
                                <tr>
                                  <th>Installment Options 3</th>
                                  <td>: {{$det?$det->installment3_options:''}}</td>
                                </tr>
                                <tr>
                                  <th>State</th>
                                  <td>: {{$det?$det->state:''}}</td>
                                </tr>
                                <tr>
                                  <th>App Version</th>
                                  <td>: {{$det?$det->app_version:''}}</td>
                                </tr>
                                <tr>
                                  <th>Launcher Version</th>
                                  <td>: {{$det?$det->launcher_version:''}}</td>
                                </tr>
                                <tr>
                                  <th>VFS Version</th>
                                  <td>: {{$det?$det->vfs_version:''}}</td>
                                </tr>
                                <tr>
                                  <th>VFSS Version</th>
                                  <td>: {{$det?$det->vfss_version:''}}</td>
                                </tr>
                                <tr>
                                  <th>Update </th>
                                  <td>: {{$det?$det->update_ts:''}}</td>
                                </tr>
                                <tr>
                                  <th>Last Diagnostic Time </th>
                                  <td>: {{$det?$det->last_diagnostic_time:''}}</td>
                                </tr>
                                <tr>
                                  <th>Last Heartbeat Time </th>
                                  <td>: {{$det?$det->last_heartbeat_time:''}}</td>
                                </tr>
                                <tr>
                                  <th>Latitude </th>
                                  <td>: {{$det?$det->latitude:''}}</td>
                                </tr>
                                <tr>
                                  <th>Longitude </th>
                                  <td>: {{$det?$det->longitude:''}}</td>
                                </tr>
                                <tr>
                                  <th>Cell Name </th>
                                  <td>: {{$det?$det->cell_name:''}}</td>
                                </tr>
                                <tr>
                                  <th>Cell Type </th>
                                  <td>: {{$det?$det->cell_type:''}}</td>
                                </tr>
                                <tr>
                                  <th>Cell Strength </th>
                                  <td>: {{$det?$det->cell_strength:''}}</td>
                                </tr>
                                <tr>
                                  <th>Push Logon </th>
                                  <td>: {{$det?$det->push_logon:''}}</td>
                                </tr>
                                <tr>
                                  <th>Host Report </th>
                                  <td>: <span  class="bool">{{$det?$det->host_report:''}}</span></td>
                                </tr>
                                <tr>
                                  <th>Host Logging </th>
                                  <td>: <span  class="bool">{{$det?$det->host_logging:''}}</span></td>
                                </tr>
                                <tr>
                                  <th>Rom Version </th>
                                  <td>: {{$det?$det->rom_version:''}}</td>
                                </tr>
                                <tr>
                                  <th>Sp Version </th>
                                  <td>: {{$det?$det->sp_version:''}}</td>
                                </tr>
                                
                                
                              </tbody>
                            </table>
                          </div>
                        </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
              </div>
        </div>


</div>
@endsection


@section('script')

<script>
   $(document).ready(function(){
    $("table>tbody>tr").each(function() { 
        $(this).find(".bool").each(function() { 
            let kl = $(this).text().trim()
            if(kl)
            {
               var k = (kl==1) ? "YES" : "NO";
               $(this).html(k);
            }
        }); 
    }); 
    });
</script>
@endsection
