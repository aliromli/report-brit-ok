@extends('auth.app')

@section('title', 'Reset Password')

@section('content')
<!-- preloader area end -->
    <!-- login area start -->
    <div class="login-area login-s2">
        <div class="container">
            <div class="login-box ptb--100">
			    <form action="{{ route('password.update') }}" id="login-form" class="smart-form client-form" method="post"> @csrf
                    <div class="login-form-head mb-3">
                        <p>Reset Password</p>
						<input type="hidden" name="token" value="{{ $token }}">
                    </div>
					<div class="form-group">
                        <label for="staticEmail" class="">Email </label>
                        <input type="text"  class="form-control form-control-sm {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autocomplete="email">
                    </div>
					@error('email')
							<span class="invalid-feedback-x" role="alert">
								<strong>{{ $message }}</strong>
							</span>
                    @enderror
					<div class="form-group">
                        <label for="password" class="">Password </label>
                        <input id="password" type="password" class="form-control form-control-sm  @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" autofocus="">

                    </div>
					 @error('password')
							<span class="invalid-feedback-x" role="alert">
								<strong>{{ $message }}</strong>
							</span>
                     @enderror
					 <div class="form-group">
                        <label for="password-confirm" class="">Confirm Password </label>
                        <input id="password-confirm" type="password" class="form-control form-control-sm " name="password_confirmation" required autocomplete="new-password">

                    </div>
					
                    <div class="form-group">
						<button id="form_submit"  type="submit" class="btn-submit" style="width:100% !important;">Reset</button>
                    </div>
					 
                    
                </form>
            </div>
        </div>
        
        <footer class="footer-login" >@ 2023 Axel Handal Trimpmitrasetia</footer>
    </div>
    
    <!-- login area end -->
@endsection
