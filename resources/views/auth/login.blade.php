@extends('auth.app')
@section('title', 'Login')
@section('content')
<div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form action="{{ route('login.action') }}" id="login-form" class="smart-form client-form" method="POST">
                @csrf
                <h1>Login Report BIT</h1>
                @if(session('success'))
                <p class="alert alert-success">{{ session('success') }}</p>
                @endif
                @if($errors->any())
                @foreach($errors->all() as $err)
                <p class="alert alert-danger">{{ $err }}</p>
                @endforeach
                @endif
              <div>
                <input type="username" name="username" class="form-control" placeholder="Username" required="" value="{{ old('username') }}" />
              </div>
              <div>
                <input type="password" name="password" class="form-control" placeholder="Password" required="" />
              </div>
              <div>
                <!-- <a class="btn btn-default submit" href="index.html">Log in</a> -->
                <button id="form_submit"  type="submit" class="btn btn-default submit">Log in</button>
                
                <!-- <a class="reset_pass" href="#">Lost your password?</a> -->
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <!-- <p class="change_link">New to site? -->
                  <!-- <a href="#signup" class="to_register"> Create Account </a> -->
                <!-- </p> -->
                <div class="clearfix"></div>
                <br />
                <div>
                  <!-- <h1><i class="fa fa-paw"></i> </h1> -->
                  <!-- <p>©2024 All Rights Reserved. </p> -->
                </div>
              </div>
            </form>
          </section>
        </div>

   
      </div>
    </div>
@endsection
