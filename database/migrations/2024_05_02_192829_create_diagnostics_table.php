<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */  
     
    public function up()
    {
        Schema::create('diagnostics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('terminal_ext_id');
            $table->text('terminal_id');
            $table->text('terminal_sn');
            $table->text('battery_temp');
            $table->text('battery_percentage');
            $table->text('latitude');
            $table->text('longitude');
            $table->text('meid');
            $table->text('switching_times');
            $table->text('swiping_card_times');
            $table->text('dip_inserting_times');
            $table->text('nfc_card_reading_times');
            $table->text('front_camera_open_times');
            $table->text('rear_camera_open_times');
            $table->text('charge_times');
            $table->text('total_memory');
            $table->text('available_memory');
            $table->text('total_flash_memory');
            $table->text('available_flash_memory');
            $table->text('total_mobile_data');
            $table->text('current_boot_time');
            $table->text('total_boot_time');
            $table->text('total_length_printed');
            $table->text('installed_apps');
            $table->text('create_ts');
            $table->text('created_by');
            //$table->string('updated_at', '50');
            //$table->string('created_at', '50');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diagnostics');
    }
};
