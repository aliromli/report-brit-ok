<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Diagnostic>
 */
class DiagnosticFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'terminal_ext_id'=> $this->faker->sentence(),
            'terminal_id' => $this->faker->sentence(),
            'terminal_sn' => $this->faker->sentence(),
            'terminal_ext_id'=> substr($this->faker->sentence(),0,10),
            'terminal_id' => substr($this->faker->sentence(),0,10),
            'terminal_sn' => substr($this->faker->sentence(),0,10),
            'battery_temp' => substr($this->faker->sentence(),0,10),
            'battery_percentage' => substr($this->faker->sentence(),0,10),
            'latitude' => substr($this->faker->sentence(),0,10),
            'longitude' => substr($this->faker->sentence(),0,10),
            'meid' => substr($this->faker->sentence(),0,10),
            'switching_times' =>substr($this->faker->sentence(),0,10),
            'swiping_card_times' => substr($this->faker->sentence(),0,10),
            'dip_inserting_times' => substr($this->faker->sentence(),0,10),
            'nfc_card_reading_times' => substr($this->faker->sentence(),0,10),
            'front_camera_open_times' => substr($this->faker->sentence(),0,10),
            'rear_camera_open_times' => substr($this->faker->sentence(),0,10),
            'charge_times' => substr($this->faker->sentence(),0,10),
            'total_memory' => substr($this->faker->sentence(),0,10),
            'available_memory' => substr($this->faker->sentence(),0,10),
            'total_flash_memory' => substr($this->faker->sentence(),0,10),
            'available_flash_memory' =>substr($this->faker->sentence(),0,10),
            'current_boot_time'  => substr($this->faker->sentence(),0,10),
            'total_mobile_data' => substr($this->faker->sentence(),0,10),
            'total_boot_time' =>substr($this->faker->sentence(),0,10),
            'total_length_printed' => substr($this->faker->sentence(),0,10),
            'installed_apps' => substr($this->faker->sentence(),0,10),
            'create_ts' => substr($this->faker->sentence(),0,10),
            'created_by' => 'Ali'
        ];
    }
}
