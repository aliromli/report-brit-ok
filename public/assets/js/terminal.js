(function($) {
    "use strict";
   
    var dataTableDiagnostic = null;
    /*dataTableDiagnostic =  $('#dataTableTerminal').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"lengthChange": false,
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/terminal-datatables", 
            type: 'GET',
            data:  function(d){
                //d.state = $('#search-state').val(); 
                //d.country = $('#search-state-country').val();
                
            }
        },
        language: {
          
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "sn", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "sn", name: "sn", visible:true},
            {data: "imei", name: "imei", visible:true},
            {data: "terminal_id", name: "terminal_id"},
            {data: "merchant_name1", name: "merchant_name1"},
            {data: "id", sortable: false, searchable: false, }
        ],

        columnDefs:[
            {
                "targets": 0, // your case first column
                "className": "text-center",
            },
            {
                targets: 5,
                "className": "text-center padd-row-table",
                render: function(d,data,row) {
                    var sn = row.sn;
                    return `<span>
                    <a href="terminal-detail/`+sn+`"  class="" data-id="`+d+`">View</a>&nbsp;
                    </span>`;
                    
                }
            }
        ]
        });
    */
    //$('#dataTableTerminal').dataTable();

    
    //$('#btn-cari-terminal').click(function() {
    //    dataTableTerminal.draw(true);
    //});

    $('body').on('click', '#export-excel-terminal', function() {
	    var data = new FormData();
		data.append('sn',1);
		$.ajax({
            url: baseUrl+"/terminal-export", 
            type: 'POST',
			contentType: false,
			processData: false,
			cache: false,
			xhrFields: {
                responseType: 'blob'
            },
			headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
			data: data,
			success: function (data, textStatus, xhr) {
                // check for a filename
                var filename = "";
                var disposition = xhr.getResponseHeader('Content-Disposition');
                if (disposition && disposition.indexOf('attachment') !== -1) {
                    var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                    var matches = filenameRegex.exec(disposition);
                    if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                    var a = document.createElement('a');
                    var url = window.URL.createObjectURL(data);
                    a.href = url;
                    a.download = filename;
                    document.body.append(a);
                    a.click();
                    a.remove();
                    window.URL.revokeObjectURL(url);
                }
                else {
                    alert("Error");
                }
               
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {

            },

        });

    });
    
   
    
})(jQuery);