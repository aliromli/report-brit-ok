(function($) {
    "use strict";
   
   
    var dataTableDiagnostic = null;
    dataTableDiagnostic =  $('#dataTableDiagnostic').DataTable({
        processing: true,
        serverSide: true,
        //dom : '',
        dom: 'lrtip',
        "searching": false,
        //"lengthChange": false,
        pageLength: 10,
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        pagingType: 'full_numbers',
        ajax: {
            url:   baseUrl+"/diagnostic-datatables", 
            type: 'GET',
            data:  function(d){
                d.state = $('#search-state').val(); 
                d.country = $('#search-state-country').val();
                
            }
        },
        language: {
           
        },
        //rowId: 'TRANSPORT_ID',
        columns: [
            {data: "id", 
               sortable: false, 
               searchable: false,
               "render": function (data, type, row, meta) {      
                          return meta.row + meta.settings._iDisplayStart + 1;     
               }  
            },
            {data: "terminal_ext_id", name: "terminal_ext_id", visible:true},
            {data: "terminal_sn", name: "terminal_sn"},
            {data: "battery_temp", name: "battery_temp"},
            {data: "battery_percentage", name: "battery_percentage"},
            {data: "id", sortable: false, searchable: false, }
        ],

        columnDefs:[
            {
                "targets": 0, // your case first column
                "className": "text-center",
            },
            {
                targets: 5,
                "className": "text-center padd-row-table",
                render: function(d,data,row) {
                  
                    return `<span>
                    <a href="diagnostic-detail/`+d+`"  class="" data-id="`+d+`">View</a>&nbsp;
                    </span>`;
                    
                }
            }
        ]
        });
       // $('#dataTableDiagnostic').DataTable();
        // $('#dataTableDiagnostic').DataTable({
        //     ajax:  baseUrl+"/diagnostic-datatables", 
        //     deferRender: true,
        //     scrollY: 380,
        //     scrollCollapse: true,
        //     scroller: true
        // });
        //
    $('body').on('click', '#export-excel', function() {

                //checkProcess(1);

                var percentage2 = 0;

                // var timer2 = setInterval(function(){
                // percentage2 = percentage2 + 5;
                // checkProcess(percentage2, timer2);
                // }, 1000);
                //checkProcess(1);

               
                var data = new FormData();
                data.append('sn',1);
                $.ajax({
                    url: baseUrl+"/diagnostic-export", 
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    cache: false,
                    xhrFields: {
                        responseType: 'blob'
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function() {
                        // setting a timeout
                        $('#div-progress').show();
                        var percentage = 0;

                        var timer = setInterval(function(){
                        percentage = percentage + 5;
                        progress_bar_process(percentage, timer);
                        }, 1000);

                    },
                    data: data,
                    success: function (data, textStatus, xhr) {
                        // check for a filename
                        //console.log("data",data)
                        //console.log("status",textStatus)
                        //console.log("xhr", xhr.getResponseHeader('Content-Disposition'))
                        //-----------
                        var filename = "";
                        var disposition = xhr.getResponseHeader('Content-Disposition');
                        if (disposition && disposition.indexOf('attachment') !== -1) {
                            var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                            var matches = filenameRegex.exec(disposition);
                            if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                            var a = document.createElement('a');
                            var url = window.URL.createObjectURL(data);
                            a.href = url;
                            a.download = filename;
                            document.body.append(a);
                            a.click();
                            a.remove();
                            window.URL.revokeObjectURL(url);
                            //console.log("data",filename)
                            $.ajax({
                                url: baseUrl+"/delete-file-export-diagnostic", 
                                type: 'POST',
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                               data: {
                                    'fileName':filename,
                                    
                                },
                                success: function(resp) {
                                    if(resp.responseCode === 200) {
                                      console.log(resp.responseMessage)
                                    } else {
                                        console.log(resp.responseMessage)
                                    }
                                    // Hide loder
                                    $('.page-loader').addClass('hidden');
                                },
                                error: function(xhr, ajaxOptions, thrownError) {
                                    console.log(xhr.statusText)
                                }
                            });
                        }
                        else {
                            alert("Error");
                        }
                    
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {

                    },

                }); 
        
        
	    

    });
    
})(jQuery);


function progress_bar_process(percentage, timer)
  {
   $('#progressbar').attr('aria-valuenow', percentage).attr('style', 'width:'+ percentage +'%');
   $('.progress-text').html(percentage+'% Completed ... Please wait ...');
   if(percentage > 100)
   {
    clearInterval(timer);
    $('#div-progress').hide();//css('display', 'none');
    $('#progressbar').attr('aria-valuenow', 0).attr('style', 'width: 0%');
    $('.progress-text').html('Export Completed');
    setTimeout(function(){
        $('#div-progress').hide();
        $('.progress-text').html('');
    }, 5000);
   }
  }

// Check process

function checkProcess(percentage, timer){

   
   if(percentage > 100)
    {
     clearInterval(timer);
    
     setTimeout(function(){
        console.log(percentage,'-',timer)
     }, 5000);
    }

}
function checkProcess_(id){
         $('#div-progress').show();
        //var url = "{{ url('process-export-diagnostic') }}"; //+id;
        
        if (typeof (EventSource) !== "undefined") {
        var process = 0;
        var url =  baseUrl+"/process-export-diagnostic";
        var source = new EventSource(url);
        source.onmessage = function (e) {

            console.log(e.data);
            if (process !== parseInt(e.data)) {
                $('#progressbar').attr('aria-valuenow', e.data).attr('style', 'width:'+ e.data+'%');
                $('.progress-text').html(e.data+'% Completed ... Please wait ...');
            }

            if(parseInt(process) === 0) {
                $('.progress-text').html('Calculate rows... Please wait ...');
            }

            if(parseInt(process) === 100) {
                
                
                // $('.input-form').removeClass('hidden');
                // $('.input-progress').addClass('hidden');
                // $('#btn-import').removeAttr('disabled');
                // $('#upload-progress').attr('aria-valuenow', 0).attr('style', 'width: 0%');
                $('#progressbar').attr('aria-valuenow', e.data).attr('style', 'width: 0%');
                // $.smallBox({
                //     title : "Success",
                //     content : 'Upload file has been finish',
                //     color : "#109618",
                //     sound_file: "voice_on",
                //     timeout: 3000
                // });
                
                //alert("Uploading Success !");

                source.close();
                
                //$('#div-progress').hide();
                
                //$("#process_id").val("");
                /*
                */


                //---
            }
            process = parseInt(e.data);
            
            
        };
        } else {
            console.log('error', 'No server-sent events support');
            $('#div-progress').show();
        }
                // var data = new FormData();
                // data.append('sn',1);
                // $.ajax({
                //     url: baseUrl+"/diagnostic-export", 
                //     type: 'POST',
                //     contentType: false,
                //     processData: false,
                //     cache: false,
                //     xhrFields: {
                //         responseType: 'blob'
                //     },
                //     headers: {
                //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                //     },
                //     data: data,
                //     success: function (data, textStatus, xhr) {
                //         // check for a filename
                //         console.log("data",data)
                //         console.log("status",textStatus)
                //         console.log("xhr", xhr.getResponseHeader('Content-Disposition'))
                //         var filename = "";
                //         var disposition = xhr.getResponseHeader('Content-Disposition');
                //         if (disposition && disposition.indexOf('attachment') !== -1) {
                //             var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                //             var matches = filenameRegex.exec(disposition);
                //             if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                //             var a = document.createElement('a');
                //             var url = window.URL.createObjectURL(data);
                //             a.href = url;
                //             a.download = filename;
                //             document.body.append(a);
                //             a.click();
                //             a.remove();
                //             window.URL.revokeObjectURL(url);
                //         }
                //         else {
                //             alert("Error");
                //         }
                    
                //     },
                //     error: function (XMLHttpRequest, textStatus, errorThrown) {

                //     },

                // }); 
}

function checkProcess2(id)
{
    // Hide all
    $('.input-form').addClass('hidden');
    $('.input-progress').removeClass('hidden');
    $('#btn-import').attr('disabled','disabled');

    // Check browser support Server-Send Event
    if (typeof (EventSource) !== "undefined") {
        // Yes! Server-sent events support!
        var process = 0;
        var url = "{{ url('update-process') }}/"+id;
        var source = new EventSource(url);
        source.onmessage = function (e) {

            mcuDataTable.ajax.reload();

            if (process !== parseInt(e.data)) {
                $('#upload-progress').attr('aria-valuenow', e.data).attr('style', 'width:'+ e.data+'%');
                $('.progress-text').html(e.data+'% Completed ... Please wait ...');
            }

            if(parseInt(process) === 0) {
                $('.progress-text').html('Calculate rows... Please wait ...');
            }

            if(parseInt(process) === 100) {

                $('.input-form').removeClass('hidden');
                $('.input-progress').addClass('hidden');
                $('#btn-import').removeAttr('disabled');
                $('#upload-progress').attr('aria-valuenow', 0).attr('style', 'width: 0%');

                $.smallBox({
                    title : "Success",
                    content : 'Upload file has been finish',
                    color : "#109618",
                    sound_file: "voice_on",
                    timeout: 3000
                });
                
                //alert("Uploading Success !");

                source.close();
                $("#process_id").val("");
            }
            
            process = parseInt(e.data);
        };
    } else {
        // Sorry! No server-sent events support..
        //toastr.error('No server-sent events support', 'Error');
        console.log('error', 'No server-sent events support');

        $('.input-form').removeClass('hidden');
        $('.input-progress').addClass('hidden');
        $('#btn-import').removeAttr('disabled','disabled');
        $("#process_id").val("");
    }
}